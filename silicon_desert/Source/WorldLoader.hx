package;

import haxe.ds.Vector;
import haxe.CallStack;

import away3d.cameras.*;
import away3d.containers.*;
import away3d.controllers.*;
import away3d.debug.*;
import away3d.entities.Mesh;
import away3d.extrusions.*;
import away3d.filters.BloomFilter3D;
import away3d.filters.DepthOfFieldFilter3D;
import away3d.lights.*;
import away3d.materials.*;
import away3d.materials.lightpickers.*;
import away3d.materials.methods.*;
import away3d.primitives.*;
import away3d.textures.*;
import away3d.utils.*;

import openfl.display.*;
import openfl.events.*;
import openfl.filters.*;
import openfl.text.*;
import openfl.ui.*;


/*
 * This class takes care the world is loaded in a given range of coordinates.
 *
 * TODO: The name of this class is misleading. We really should rename it.
 *
 * This might be the class to apply all operators to a virgin void world.
 * It should have the standard "behave" interface. However, when provide fragments
 * is called, it should propage the call to the individual operators.
 *
 * The terrain container keeps references to the world meta generator
 * and the actual 3d scene.
 *
 * TODO:
 * This data structure needs to keep the actual objects and global data structures
 + of the world scene. We need to define what compoonents this data structure is made
 * of, e.g.:
 * - terrain heights
 * - objects lists
 * - cluster lists
 * - behavioural objects
 */ 
class WorldLoader
{
    private var _worldMetaGen:WorldMetaGen;
    private var _allEnv:AllEnv;

    private var _scene:Scene3D;

    // TXWTODO: Create a factorized cube material injected by object.
    public var cubeMaterial:TextureMaterial;

    private var _fragCurrent:WorldFragment;

    private var _strLastLoaded:String = "";
    private var _lastLoadedIteration:Int = 0;
    private var _mapFrags:Map<String,WorldFragment>;


    public function worldLoaderBehave() : Void
    {
        var lsKill:List<ICharacter> = new List<ICharacter>();
        for( strKey in _mapFrags.keys() ) {
            var frag = _mapFrags[strKey];
            frag.worldFragmentBehaveStatic();
        }
        for( strKey in _mapFrags.keys() ) {
            var frag = _mapFrags[strKey];
            frag.worldFragmentBehave();
        }
    }


    /**
     * Instruct the factory to obtain data for at least the bounding box
     * given, for a viewer nat 
     */     
    public function worldLoaderRequireBox( 
        vecPosition:Vector<Float>,
        vecBoundingBox:Vector<Float>  )
    {
    }


    /**
     * Load the fragments required for a given position into the 
     * current world.
     *
     * This function triggers application of all operators required for
     * that specific world fragment.
     *
     * TODO:
     * - we also should incremental loading (for LoD or networked connections),
     */
    public function worldLoaderProvideFragments( x:Float, y:Float, z:Float )
    {
        // trace('worldLoaderProvideFragments: Called $x, $y, $z.');

        x += WorldMetaGen.fragmentSize/2;
        y += WorldMetaGen.fragmentSize/2;
        z += WorldMetaGen.fragmentSize/2;
        x /= WorldMetaGen.fragmentSize;
        y /= WorldMetaGen.fragmentSize;
        z /= WorldMetaGen.fragmentSize;

        // Rest y to 0, we don't use heights now.
        y = 0;

        var i:Int = Math.floor( x );
        var j:Int = Math.floor( y );
        var k:Int = Math.floor( z );

        //trace( [x, y, z] );
        //trace( [i, j, k ] );

        var strCurr = "fragxy-"+i+"_"+j+"_"+k;

        /*
         * Create a list of maps 
         */ 
        if( _strLastLoaded == strCurr ) {
            // No need to load something new.
            return;
        }
        _strLastLoaded = strCurr;
        ++_lastLoadedIteration;

        trace( "worldLoaderProvideFragments: Entered new terrain "+strCurr+", loading." );
        for( dz in -2...3 ) {
            for( dx in -2...3 ) {
                var i1:Int = i + dx;
                var j1:Int = j;
                var k1:Int = k + dz;
                var strKey = "fragxy-"+i1+"_"+j1+"_"+k1;
                trace("Loading "+strKey);   

                /*
                 * Look, wether the corresponding fragment still is in the
                 * cache, or wether we need to load (i.e. generate) it.
                 */
                var fragment:WorldFragment = _mapFrags.get( strKey );
                if( null != fragment ) {
                    // Mark as used.
                    // trace( "Using "+strKey );
                    fragment.lastIteration = _lastLoadedIteration;
                } else {

                    /*
                     * This creates a new world fragment.
                     *
                     * World fragments are the containers for everything that
                     * comes on that please of the world. When they are created,
                     * they basically contains a canvas, in which the world is
                     * created.
                     */
                    fragment = new WorldFragment( 
                        _allEnv,
                        strKey,
                        i1, j1, k1, 
                        WorldMetaGen.fragmentSize * i1,
                        WorldMetaGen.fragmentSize * j1,
                        WorldMetaGen.fragmentSize * k1 );
                    fragment.lastIteration = _lastLoadedIteration;


                    /*
                     * The following operators already might want to access this client. 
                     */
                    _mapFrags.set( strKey, fragment );

                    /*
                     * Before we apply any operators, load static structures.
                     */
                    fragment.worldFragmentLoadStatic( _allEnv );

                    /*
                     * Apply all fragment operators.
                     */
                    _allEnv.worldMetaGen.applyFragmentOperators(
                        _allEnv, fragment);
                    _mapFrags.set( strKey, fragment );

                    fragment.worldFragmentPopulate();
                    fragment.worldFragmentAdd( _scene, 1.0 );
                }
            }
        }
        _fragCurrent = _mapFrags[strCurr];

        // Now erase the fragments we do not need any more.
        for( strKey in _mapFrags.keys() ) {
            var frag = _mapFrags[strKey];
            if( frag.lastIteration < _lastLoadedIteration ) {
                // trace( "discarding "+strKey );
                frag.worldFragmentRemove( _scene );
                frag.worldFragmentUnload();
                _mapFrags.remove( strKey );
                frag = null;
            } else {
                // trace( "keeping "+strKey );
            }
        }
    }


    /**
     * Return the default walking height for this position.
     * Return the height at the given position of the world.
     * If the position is unknown, it might very well be 0.0.
     * This method does not force loading the particular are of 
     * the world.
     *
     * TXWTODO: This does not require the entire worldLoader 
     * but only the elevation.
     */
    public function worldLoaderGetHeightAt( 
        x0:Float,
        z0:Float,
        layer: String = elevation.Cache.TOP_LAYER
    ):Float
    {
        /*
         * Convert global  to fragment-local coordinates
         */
        var x:Float = x0+WorldMetaGen.fragmentSize/2;
        var z:Float = z0+WorldMetaGen.fragmentSize/2;
        x /= WorldMetaGen.fragmentSize;
        z /= WorldMetaGen.fragmentSize;
 
        var i:Int = Math.floor( x );
        var k:Int = Math.floor( z );

        // TXWTODO: find a more suitable "new" API for this.
        var elevationCache = cast( WorldMetaGen.cat.catGetEntity('elevation.Cache'), elevation.Cache );
        var entry = elevationCache.elevationCacheGetBelow(i, k, layer);

        var localX:Float = x0 - (WorldMetaGen.fragmentSize)*i;
        var localZ:Float = z0 - (WorldMetaGen.fragmentSize)*k;

        var height:Float = entry.getHeightAt( localX, localZ );

        return height;
    }


    /**
     * Initialise the material
     */
    private function initMaterials():Void
    {
        cubeMaterial = new TextureMaterial( Cast.bitmapTexture( "cube1.jpg" ) );
        cubeMaterial.lightPicker = _allEnv.allEnvironmentGetLightPicker();
        cubeMaterial.ambientColor = 0x808080;
        cubeMaterial.ambient = 1;
        cubeMaterial.specular = .2;
        cubeMaterial.repeat = true;
        cubeMaterial.addMethod( _allEnv.allEnvironmentGetFogMethod() );
    }


    public function terrainEnv() : AllEnv
    {
        return _allEnv;
    }

    
    /**
     * Initialise the scene objects
     */
    private function initObjects():Void
    {
    }


    /**
     * Global initialise function
     */
    private function init():Void
    {
        _mapFrags = new Map<String,WorldFragment>();

        _allEnv = new AllEnv( _worldMetaGen, this );

        /*
         * Now, as the terrain container is available, we can ask the
         * terrain environment to load the materials.
         */
        _allEnv.allEnvironmentLoadStatic();       
        _allEnv.allEnvironmentAdd( _scene, 1.0 );

        initMaterials();
        initObjects();
    }


    /**
     * Constructor
     */
    public function new(
            worldMetaGen0:WorldMetaGen,
            scene0:Scene3D )
    {
        _worldMetaGen = worldMetaGen0;
        _scene = scene0;
        init();
    }
    

}