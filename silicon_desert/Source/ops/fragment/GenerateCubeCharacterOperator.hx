package ops.fragment;

import openfl.geom.Vector3D;

import streets.StreetPoint;

class GenerateCubeCharacterOperator implements IFragmentOperator {

    private var _clusterDesc: ClusterDesc;
    private var _rnd: RandomSource;
    private var _myKey: String;


    public function fragmentOperatorGetPath(): String
    {
        return '7001/GenerateCubeCharacterOperatar/$_myKey/';
    }


    public function fragmentOperatorGetBoundingBox(
        allEnv: AllEnv,
        worldFragment: WorldFragment
    ) : Array<Vector3D> {
        var bbox = new Array<Vector3D>();
        bbox[0] = new Vector3D(
            _clusterDesc.x - _clusterDesc.size,
            _clusterDesc.y - 1000,
            _clusterDesc.z - _clusterDesc.size
        );
        bbox[1] = new Vector3D(
            _clusterDesc.x + _clusterDesc.size,
            _clusterDesc.y + 1000,
            _clusterDesc.z + _clusterDesc.size
        );
        return bbox;
    }


    public function fragmentOperatorApply(
        allEnv: AllEnv,
        worldFragment: WorldFragment
    ) : Void {
        // Perform clipping until we have bounding boxes

        var cx:Float = _clusterDesc.x - worldFragment.x;
        var cz:Float = _clusterDesc.z - worldFragment.z;

        /*
         * We don't apply the operator if the fragment completely is
         * outside our boundary box (the cluster)
         */
        {
            {
                var csh: Float = _clusterDesc.size / 2.0;
                var fsh: Float = WorldMetaGen.fragmentSize / 2.0;            
                if (
                    (cx-csh)>(fsh)
                    || (cx+csh)<(-fsh)
                    || (cz-csh)>(fsh)
                    || (cz+csh)<(-fsh)
                ) {
                    // trace( "Too far away: x="+_clusterDesc.x+", z="+_clusterDesc.z);
                    return;
                }
            }
        }

        trace( 'GenerateCubeCharacterOperator(): cluster "${_clusterDesc.id}" (${_clusterDesc.x}, ${_clusterDesc.z}) in range');
        _rnd.clear();

        /*
         * Now, that we have read the cluster description that is associated, we
         * can place the characters randomly on the streetpoints.
         *
         * TXWTODO: We would be more intersint to place them on the streets.
         */

        var i:Int;
        var strokeStore = _clusterDesc.strokeStore();
        var streetPoints = strokeStore.getStreetPoints();
        var l = streetPoints.length;
        for( i in 0...200 ) {
            var idxPoint = Std.int(_rnd.getFloat()*l);
            // TXWTODO: Data structure!!!!
            var idx = 0;
            var chosenStreetPoint: StreetPoint = null;
            for( sp in streetPoints ) {
                if( idx==idxPoint ) {
                    chosenStreetPoint = sp;
                    break;
                }
                idx++;
            }
            /*
             * Check, wether the given street point really is inside our fragment.
             * That way, every fragment owns only the characters spawn on their
             * territory.
             */
            {
                var px = chosenStreetPoint.pos.x + _clusterDesc.x;
                var pz = chosenStreetPoint.pos.y + _clusterDesc.z;
                if( !worldFragment.isInside( px, pz )) {
                    chosenStreetPoint = null;
                }
            }
            if( null != chosenStreetPoint ) {
                trace( 'GenerateCubeCharacterOperator(): Starting on streetpoint $idxPoint ${chosenStreetPoint.pos.x}, ${chosenStreetPoint.pos.y}.');
                // TXWTODO: Roll the dice to get streetpoint.
#if 1
                var cube = new CubeCharacter(
                    allEnv, _clusterDesc, chosenStreetPoint );
                worldFragment.worldFragmentAddCharacter( cube );
#end
            } else {
                // trace( "GenerateCubeCharacterOperator(): No streetpoint found." );
            }
        }

    }


    public function new (
        clusterDesc: ClusterDesc,
        strKey: String
    ) {
        _clusterDesc = clusterDesc;
        _myKey = strKey;
        _rnd = new RandomSource(strKey);
    }
}