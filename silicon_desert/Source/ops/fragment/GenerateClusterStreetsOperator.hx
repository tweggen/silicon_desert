package ops.fragment;

import openfl.geom.Vector3D;
import openfl.Vector;
import away3d.entities.Mesh;
import away3d.core.base.Geometry;
import away3d.core.base.SubGeometry;

class GenerateClusterStreetsOperator implements IFragmentOperator {

    private var _clusterDesc: ClusterDesc;
    private var _rnd: RandomSource;
    private var _myKey: String;
    private var _traceStreets = false;

    private function p(
        a: Vector<Float>, 
        x: Float, y: Float, z: Float
    ): Void {
        a.push(x);
        a.push(y);
        a.push(z);
    }

    private function uv(
        a: Vector<Float>, 
        u: Float, v: Float
    ): Void {
        a.push(u);
        a.push(v);
    }

    private function idx(
        a: Vector<UInt>, 
        p1: UInt, p2: UInt, p3: UInt
    ): Void {
        a.push(p1);
        a.push(p2);
        a.push(p3);
    }


    public function fragmentOperatorGetPath(): String
    {
        return '5001/GenerateClusterStreetsOperator/$_myKey/';
    }


    /**
     * This fragment operator extends as much as the cluster does it
     * shall bring to life.
     */
    public function fragmentOperatorGetBoundingBox(
        allEnv: AllEnv,
        worldFragment: WorldFragment
    ) : Array<Vector3D> {
        var bbox = new Array<Vector3D>();
        bbox[0] = new Vector3D(
            _clusterDesc.x - _clusterDesc.size,
            _clusterDesc.y - 1000,
            _clusterDesc.z - _clusterDesc.size
        );
        bbox[1] = new Vector3D(
            _clusterDesc.x + _clusterDesc.size,
            _clusterDesc.y + 1000,
            _clusterDesc.z + _clusterDesc.size
        );
        return bbox;
    }


    /**
     * Create meshes for all street strokes with their "A" StreetPoint in this fragment.
     */
    public function fragmentOperatorApply(
        allEnv: AllEnv,
        worldFragment: WorldFragment
    ) : Void {
        // Perform clipping until we have bounding boxes

        var cx:Float = _clusterDesc.x - worldFragment.x;
        var cz:Float = _clusterDesc.z - worldFragment.z;

        /*
         * We don't apply the operator if the fragment completely is
         * outside our boundary box (the cluster)
         */
        {
            {
                var csh: Float = _clusterDesc.size / 2.0;
                var fsh: Float = WorldMetaGen.fragmentSize / 2.0;            
                if (
                    (cx-csh)>(fsh)
                    || (cx+csh)<(-fsh)
                    || (cz-csh)>(fsh)
                    || (cz+csh)<(-fsh)
                ) {
                    // trace( "Too far away: x="+_clusterDesc.x+", z="+_clusterDesc.z);
                    return;
                }
            }
        }

        trace( 'GenerateClusterStreetsOperator(): cluster "${_clusterDesc.name}" (${_clusterDesc.id}) in range');
        if( _traceStreets ) trace( 'GenerateClusterStreetsOperator(): Obtaining streets.');
        var strokeStore = _clusterDesc.strokeStore();
        if( _traceStreets ) trace( 'GenerateClusterStreetsOperator(): Have streets.');

        if( _traceStreets ) trace('GenerateClusterStreetsOperator(): In terrain "${worldFragment.getId()}" operator. '
            + 'Fragment @${worldFragment.x}, ${worldFragment.z}. '
            + 'Cluster "${_clusterDesc.id}" @$cx, $cz, R:${_clusterDesc.size}.');

        var matStreet = WorldMetaGen.cat.catGetSingleton( 
            "GenerateClusterStreetsOperator._matStreet", function () {
                return new materials.StreetMaterial(allEnv);
            }).texGet();

        /*
         * We need the coordinates of the cluster relative to the fragment to translate 
         * everything to fragment coorddinates.
         */

        var nGeneratedStreets = 0;
        var nIgnoredStrokes = 0;

        var geo = new Geometry();
        for( stroke in strokeStore.getStrokes()) {

            /*
             * We store the street map relative to the center of the cluster.
             * So check the points relative to it. At this point we already know that 
             * there we are kind of intersecting the cluster.
             */

            var spA = stroke.a;
            var sax = cx + spA.pos.x;
            var saz = cz + spA.pos.y;
            if( !worldFragment.isInsideLocal( sax, saz ) ) {
                // Ignoring stroke, it is not inside my range.
                nIgnoredStrokes++;
                continue;
            }
            var spB = stroke.b;
            var sbx = cx + spB.pos.x;
            var sbz = cz + spB.pos.y;

            /*
             * In addition to these endpoints we also need vectors
             * in direction of the stroke and perpendicular with a length
             * of (half) the street width.
             *
             * For the time being, we assume the width is half of the actual width;
             */

            // Linear Unit X/Z
            var lux = (sbx-sax) / stroke.length;
            var luz = (sbz-saz) / stroke.length;
            // Create 90deg
            var pux = luz;
            var puz = -lux;

            // strokewidth currently ranges from 0.5 to 1.5, with most branches around 0.65
            // With small streets being 6m, standard around 8m, we scale like this:
            var streetWidth = stroke.weight * (6./0.5);
            // trace( 'streetWidth = $streetWidth' );
            // Now, adjusted to the width of the street, using this preliminary width.
            var halfStreetWidth = streetWidth / 2.0;
            var lwx = lux * halfStreetWidth;
            var lwz = luz * halfStreetWidth;

            var pwx = pux * halfStreetWidth;
            var pwz = puz * halfStreetWidth;

            var h = _clusterDesc.averageHeight + 2.0;

            /*
             * Create a new geometry from sa to sb at the average height of the cluster + 0.5.
             *
             * So, clockwise, this is a rectangle from a-lw+pw, a-lw-pw, b+lw-pw, b+lw+pw
             */
            {
                var subgeo = new SubGeometry();
                subgeo.autoDeriveVertexNormals = true;
                subgeo.autoDeriveVertexTangents = true;
                geo.addSubGeometry( subgeo );
                var u0: Float;
                var u1: Float;
                if( stroke.weight<1.1 ) {
                    u0 = 0.0; u1 = 0.25;
                } else {
                    u0 = 0.5; u1 = 0.75;
                }

                var v = new Vector<Float>();
                var i = new Vector<UInt>();
                var u = new Vector<Float>();

                // See the comment above.
                p(v, sax-lwx+pwx, h, saz-lwz+pwz);
                uv(u, u1, u0);
                p(v, sax-lwx-pwx, h, saz-lwz-pwz);
                uv(u, u0, u0);
                p(v, sbx+lwx-pwx, h, sbz+lwz-pwz);
                uv(u, u0, u1);
                p(v, sbx+lwx+pwx, h, sbz+lwz+pwz);
                uv(u, u1, u1);

                // Two triangles

                // Wrong: Counter clock:
                idx(i, 0, 1, 2);
                // Right: Clockwise. We should be able to see at least one of them.
                idx(i, 0, 2, 3);

                subgeo.updateVertexData( v );
                subgeo.updateIndexData( i );
                subgeo.updateUVData( u );


                ++nGeneratedStreets;
            }
        }
        var mesh = new Mesh( geo, matStreet );
        worldFragment.addMesh( mesh );
        trace('GenerateClusterStreetsOperator(): Created $nGeneratedStreets strokes, discarded $nIgnoredStrokes.');
    }
    

    public function new (
        clusterDesc: ClusterDesc,
        strKey: String
    ) {
        _clusterDesc = clusterDesc;
        _myKey = strKey;
        _rnd = new RandomSource(strKey);
    }
}
