package ops.fragment;

import openfl.geom.Vector3D;

class GenerateHousesOperator implements IFragmentOperator {

    private var _clusterDesc: ClusterDesc;
    private var _rnd: RandomSource;
    private var _myKey: String;

    public function fragmentOperatorGetPath(): String
    {
        return '8001/GenerateHousesOperator/$_myKey/';
    }


    /**
     * This fragment operator extends as much as the cluster does it
     * shall bring to life.
     */
    public function fragmentOperatorGetBoundingBox(
        allEnv: AllEnv,
        worldFragment: WorldFragment
    ) : Array<Vector3D> {
        var bbox = new Array<Vector3D>();
        bbox[0] = new Vector3D(
            _clusterDesc.x - _clusterDesc.size,
            _clusterDesc.y - 1000,
            _clusterDesc.z - _clusterDesc.size
        );
        bbox[1] = new Vector3D(
            _clusterDesc.x + _clusterDesc.size,
            _clusterDesc.y + 1000,
            _clusterDesc.z + _clusterDesc.size
        );
        return bbox;
    }


    public function fragmentOperatorApply(
        allEnv: AllEnv,
        worldFragment: WorldFragment
    ) : Void {
        var cx:Float = _clusterDesc.x - worldFragment.x;
        var cz:Float = _clusterDesc.z - worldFragment.z;

        //var clusterFragment = _clusterDesc.fragment;

        var fsh: Float = WorldMetaGen.fragmentSize / 2.0;            

        // Perform clipping until we have bounding boxes
        /*
         * We don't apply the operator if the fragment completely is
         * outside our boundary box (the cluster)
         */
        {
            var csh: Float = _clusterDesc.size / 2.0;
            if (
                (cx-csh)>(fsh)
                || (cx+csh)<(-fsh)
                || (cz-csh)>(fsh)
                || (cz+csh)<(-fsh)
            ) {
                // trace( "Too far away: x="+_clusterDesc.x+", z="+_clusterDesc.z);
                return;
            }
        }

        trace( 'GenerateHousesOperator(): cluster "${_clusterDesc.name}" (${_clusterDesc.id}) in range');
        _rnd.clear();
#if 0
        trace('GenerateHousesOperator(): In terrain "${worldFragment.getId()}" operator. '
            + 'Fragment @${worldFragment.x}, ${worldFragment.z}. '
            + 'Cluster "${_clusterDesc.id}" @$clusterInFragmentX, $clusterInFragmentZ, R:${_clusterDesc.size}.');
#end

        /*
         * Iterate through all quarters in the clusters and generate lots and houses.
         */
        var quarterStore = _clusterDesc.quarterStore();
        var i = 0;
        for( quarter in quarterStore.getQuarters() ) {
            /*
             * Place on house in each quarter in the middle.
             */
            var xmiddle = 0.0;
            var ymiddle = 0.0;
            var n = 0;
            for( delim in quarter.getDelims() ) {
                xmiddle += delim.streetPoint.pos.x;
                ymiddle += delim.streetPoint.pos.y;
                ++n;
            }
            if( 0==n ) {
                continue;
            }
            xmiddle /= n;
            ymiddle /= n;

            /*
             * We would place the house at xmiddle / ymiddle
             * If this is outside of the cluster, we do not generate the house.
             */
            if(!worldFragment.isInsideLocal( cx+xmiddle, cz+ymiddle ) ) {
                continue;
            }
            var inFragmentX = xmiddle+cx;
            var inFragmentZ = ymiddle+cz;
#if 1
            var o3dHouse = new PlaceObject( _myKey+"-house"+i, worldFragment );
            o3dHouse.thingSetup(allEnv);
            o3dHouse.thingLoad();
            o3dHouse.thingPlace( 
                worldFragment.x, worldFragment.y, worldFragment.z,
                inFragmentX,
                0, // thingInFragmentY
                inFragmentZ );
            worldFragment.addPlaceObject(o3dHouse);
#end
            ++i;
        }

    }
    

    public function new (
        clusterDesc: ClusterDesc,
        strKey: String
    ) {
        _clusterDesc = clusterDesc;
        _myKey = strKey;
        _rnd = new RandomSource(strKey);
    }
}
