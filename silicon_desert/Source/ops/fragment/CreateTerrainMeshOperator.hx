package ops.fragment;

import openfl.geom.Vector3D;

class CreateTerrainMeshOperator 
    implements IFragmentOperator 
{

    // private var _rnd: RandomSource;
    private var _myKey: String;


    public function fragmentOperatorGetPath(): String
    {
        // TXWTODO: The mesh should be generated very late, after everybody applied their terrain operators.
        return '4001/CreateTerrainMeshOperator/$_myKey/';
    }


    /**
     * This fragment operator spans exactly over the fragment it
     * shall run on.
     */
    public function fragmentOperatorGetBoundingBox(
        allEnv: AllEnv,
        worldFragment: WorldFragment
    ) : Array<Vector3D> {
        var worldMetaGen = allEnv.worldMetaGen;

        var bbox = new Array<Vector3D>();
        bbox[0] = new Vector3D(
            worldFragment.x,
            worldFragment.y - 1000,
            worldFragment.z
        );
        bbox[1] = new Vector3D(
            worldFragment.x + WorldMetaGen.fragmentSize,
            worldFragment.y + 1000,
            worldFragment.z + WorldMetaGen.fragmentSize
        );
        return bbox;
    }


    /**
     * Load the final elevation table from the elevation cache and
     * apply it to the world fragment.
     */
    public function fragmentOperatorApply(
        allEnv: AllEnv,
        worldFragment: WorldFragment
    ) : Void {
        var worldMetaGen = allEnv.worldMetaGen;

        // _rnd.clear();

        /*
         * Load the final elevation for that fragment from the elevation cache.
         * If not done before, this will trigger computing the elevation levels.
         * but most likely, some other operator will already have triggered this.
         */
        {
            var elevationCache = cast( WorldMetaGen.cat.catGetEntity('elevation.Cache'), elevation.Cache );
            var i = worldFragment.i;
            var k = worldFragment.k;
#if 1
            /*
             * This would be the right way, however, it's more expensive.
             */
            var fs = WorldMetaGen.fragmentSize;
            var x0: Float = i*fs - fs/2.0;
            var z0: Float = k*fs - fs/2.0;
            var x1: Float = (i+1)*fs - fs/2.0;
            var z1: Float = (k+1)*fs - fs/2.0;

            var elevationRect = elevationCache.elevationCacheGetRectBelow(
                x0, z0, x1, z1, elevation.Cache.TOP_LAYER
            );
            worldFragment.worldFragmentSetGroundArray(
                elevationRect.elevations,
                WorldMetaGen.groundResolution,
                0, 0, WorldMetaGen.groundResolution, WorldMetaGen.groundResolution, 
                0, 0);
#else
            /*
             * Reading straight from the cache is not the clean way, however,
             * it is faster for daily use.
             */
            var elevationCacheEntry = elevationCache.elevationCacheGetBelow( i, k, elevation.Cache.TOP_LAYER );
            if( null==elevationCacheEntry ) {
                throw 'worldLoaderProvideFragments: i:$i, k:$k: returned null.';
            }
            worldFragment.worldFragmentSetGroundArray(
                elevationCacheEntry.elevations,
                WorldMetaGen.groundResolution,
                0, 0, WorldMetaGen.groundResolution, WorldMetaGen.groundResolution, 
                0, 0);
#end
        }

        /*
         * Create geometry from the elevations stored in the fragment.
         */
        {
            worldFragment.worldFragmentLoadGround();
        }
    }


    public function new (
        strKey: String
    ) {
        _myKey = strKey;

        // We don't need a seed yet. I leave it in the code in case we
        // need it later on.
        // _rnd = new RandomSource(strKey);
    }
}
