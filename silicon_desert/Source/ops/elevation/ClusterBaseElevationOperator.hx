package ops.elevation;


class ClusterBaseElevationOperator 
    implements elevation.Operator
{

    /**
     * This is the city cluster I am associated with.
     */ 
    private var _clusterDesc: ClusterDesc;

    private var _strKey: String;

    /**
     * Test operator to test operator chaining. This operator evens
     * out everything below the average height of a city to the average.
     *
     * Doesn't really make sense, just a test.
     */ 
    public function elevationOperatorProcess( 
        elevationInterface: elevation.Interface,
        erTarget: elevation.Rect 
    ) : Void {

        // trace('ClusterBaseElevationOperator(): Operating.');
        // var fs = WorldMetaGens.fragmentSize;

        var x0 = _clusterDesc.x - _clusterDesc.size / 2.0;
        var z0 = _clusterDesc.z - _clusterDesc.size / 2.0;
        var x1 = _clusterDesc.x + _clusterDesc.size / 2.0;
        var z1 = _clusterDesc.z + _clusterDesc.size / 2.0;

        // TXWTODO: Cache this in the cluster?
        var erCluster = elevationInterface.getElevationRectBelow( x0, z0, x1, z1 );
        var aver = 0.0;
        for( cez in 0...erCluster.nVert ) {
            for( cex in 0...erCluster.nHoriz ) {
                aver += erCluster.elevations[cez][cex];
            }
        }
        aver /= erCluster.nHoriz * erCluster.nVert;
        _clusterDesc.averageHeight = aver;

        /*
         * Now that we have the average, read the level below us.
         */
        var erSource = elevationInterface.getElevationRectBelow( 
            erTarget.x0, erTarget.z0,
            erTarget.x1, erTarget.z1 
        );

        /*
         * Now we iterate through our target, filling it with the
         * data from source, modifying the data along the way.
         */

        /*
         * Copy data from source to target, modifying it.
         *
         * In this version we only apply the change to values within our
         * city bounds.
         */
        for( tez in 0...erTarget.nVert ) {
            var z = erTarget.z0 
                + ((erTarget.z1-erTarget.z0) * tez)
                    / erTarget.nVert;

            for( tex in 0...erTarget.nHoriz ) {
                /*
                 * Compute the absolute position derived from the target
                 * coordinates.
                 *
                 * Then check, wether this is within the bounds of the 
                 * city.
                 */ 
                var x = erTarget.x0 
                    + ((erTarget.x1-erTarget.x0) * tex)
                        / erTarget.nHoriz;

                var resultHeight: Float;
                var sourceHeight = erSource.elevations[tez][tex];

                if (true
                    && x >= x0
                    && x <= x1
                    && z >= z0
                    && z <= z1 
                ) {
                    /*
                     * This is wihtin the range of our city. 
                     * So flatten it.
                     */
                    if( sourceHeight<aver ) {
                        resultHeight = aver - 0 ;
                    } else {
                        resultHeight = aver + 1.5;
                    }
                } else {
                    /*
                     * This is not within the range of our city.
                     */
                    resultHeight = sourceHeight;
                }
                erTarget.elevations[tez][tex] = resultHeight;
            }
        }
    }


    public function elevationOperatorIntersects(
        x0: Float, z0: Float,
        x1: Float, z1: Float
    ) : Bool {
        var sh = _clusterDesc.size / 2.0;
        var cx0 = _clusterDesc.x - sh;
        var cz0 = _clusterDesc.z - sh;
        var cx1 = _clusterDesc.x + sh;
        var cz1 = _clusterDesc.z + sh;
        if( 
            false 
            || x0 > cx1 
            || z0 > cz1
            || x1 < cx0
            || z1 < cz0
        ) {
            return false;
        }
        return true;
    }


    public function new (
        clusterDesc: ClusterDesc,
        strKey: String
    ) {
        _clusterDesc = clusterDesc;
        _strKey = strKey;
        // _rnd = new RandomSource(strKey);
    }
}


