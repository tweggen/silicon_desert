package ops.elevation;

/**
 * Elevation factory for basic random grounds.
 * This one takes the cornerstone elevations as provided by WorldMetaGen,
 * creates reproducable subdivisions and does not modify them otherwise.
 *
 * It has a static function that can be used as a factory function for
 * the elevation.Cache.
 */
class ElevationBaseFactory 
    implements elevation.Operator    
{
    private var _worldMetaGen: WorldMetaGen;

    /// World width
    var _maxWidth:Float;
    /// World height
    var _maxHeight:Float;
    /// Number of indices horizontally in world.
    var _skeletonWidth:Int;
    /// Number of indices vertically (depth) in world.
    var _skeletonHeight:Int;


    private function createSeed( x: Int, y: Int ) : String {
        return "fragxy-"+x+"_0_"+y;
    }


    /**
     * Compute the actual elevation array for a given grid entry.
     */
    private function createElevationCacheEntry(
        i: Int, k: Int,
        elevationRect: elevation.Rect
    ): Void {

        var groundOperator: GroundOperator = 
            cast(WorldMetaGen.cat.catGetEntity("GroundOperator"), GroundOperator);
        var skeletonElevations = groundOperator.getSkeleton();

        /*
         * Create a local array for computing the elevations. By convention,
         * it also includes the individual borders.
         */
        var nElevations = WorldMetaGen.groundResolution+1;
        var localElevations:Array<Array<Float>> = elevationRect.elevations;

        /*
         * First setup the corners from the skeleton information.
         */

        /*
         * We try an API with the direct index coordinates for our global
         * precomputed elevation index.
         */
        var idxX:Int = Std.int( 
            (i * WorldMetaGen.fragmentSize + _maxWidth/2.0)
                / WorldMetaGen.fragmentSize );
        var idxY:Int = Std.int( 
            (k * WorldMetaGen.fragmentSize + _maxHeight/2.0)
                / WorldMetaGen.fragmentSize );

        var x0:Int = 0;
        var y0:Int = 0;
        var x1:Int = x0+nElevations-1;
        var y1:Int = y0+nElevations-1;

        if(true) {
            if( null==localElevations ) {
                throw 'localElevations is null';
            }
            if( null==skeletonElevations ) {
                throw 'skeletonElevations is null';
            }
            if(y0<0) {
                throw 'y0 < 0';
            }
            if(y1<0) {
                throw 'y1 < 0';
            }
            if(x0<0) {
                throw 'x0 < 0';
            }
            if(x1<0) {
                throw 'x1 < 0';
            }
            var leLength = localElevations.length;
            if(y0>=leLength) {
                throw 'y0=$y0 >= leLength=$leLength';
            }
            if(y1>=leLength) {
                throw 'y1=$y1 >= leLength=$leLength';
            }
            var ley0Length = localElevations[y0].length;
            if( x0>=ley0Length ) {
                throw 'x0=$x0 >= ley0Length=$ley0Length';
            }
            if( x1>=ley0Length ) {
                throw 'x1=$x1 >= ley0Length=$ley0Length';
            }
        }
        if(true) {
            var seLength = skeletonElevations.length;
            if(idxX<0) {
                throw 'idxX<0';
            }
            if(idxY<0) {
                throw 'idxY<0';
            }
            if(idxY >= seLength) {
                throw 'idxY = $idxY >= seLength = $seLength';
            }
            idxY++;
            if(idxY >= seLength) {
                throw 'idxY+1 = $idxY >= seLength = $seLength';
            }
            var seyLength = skeletonElevations[idxY-1].length;
            if(idxX >= seyLength) {
                throw 'idxX = $idxX >= seyLength = $seyLength';
            }
            idxX++;
            if(idxX >= seyLength) {
                throw 'idxX+1 = $idxX >= seyLength = $seyLength';
            }
            --idxX;
            --idxY;
        }

        // trace('idxY $idxY idxX $idxX _skeletonHeight $_skeletonHeight');
        // trace(skeletonElevations.length);
        // trace(skeletonElevations[idxY+1].length);
        localElevations[y0][x0] = skeletonElevations[idxY][idxX];
        localElevations[y0][x1] = skeletonElevations[idxY][idxX+1];
        localElevations[y1][x0] = skeletonElevations[idxY+1][idxX];
        localElevations[y1][x1] = skeletonElevations[idxY+1][idxX+1];

        /*
         * Compute all remaining elevations.
         */
        elevation.Tools.refineSkeletonElevation(
            idxX, idxY,
            localElevations,
            _worldMetaGen.minElevation,
            _worldMetaGen.maxElevation,
            x0, y0, x1, y1 );

        /*
         * Everything is computed in-place into the target rectangle.
         * So we directly can return.
         */
#if 0
        /*
         * Create the actual cache entry.
         */
        var elevationCacheEntry = new elevation.CacheEntry(_worldMetaGen);
        elevationCacheEntry.elevations = localElevations;
        return elevationCacheEntry;
#end
    }


    public function elevationOperatorProcess(
        elevationInterface: elevation.Interface,
        target: elevation.Rect
    ): Void {
        /*
         * Copute the world fragment index- Remember we can asume this
         * exactly is one world fragment.
         */
        // TXWTODO: Create a convenicence function for this.
        var fs = WorldMetaGen.fragmentSize;
        var i: Int = Math.floor((target.x0+fs/2.0)/fs);
        var k: Int = Math.floor((target.z0+fs/2.0)/fs);
        
        createElevationCacheEntry( i, k, target );
    }


    public function elevationOperatorIntersects(
        x0: Float, z0: Float,
        x1: Float, z1: Float
    ) : Bool {
        return true;
    }


    public function new (worldMetaGen: WorldMetaGen) {
        _worldMetaGen = worldMetaGen;

        _maxWidth = _worldMetaGen.maxWidth;
        _maxHeight = _worldMetaGen.maxHeight;

        var fragmentSize = WorldMetaGen.fragmentSize;

        _skeletonWidth = Std.int( (_maxWidth+fragmentSize-1)/fragmentSize ) + 1;
        _skeletonHeight = Std.int( (_maxHeight+fragmentSize-1)/fragmentSize ) + 1;
    }
}
