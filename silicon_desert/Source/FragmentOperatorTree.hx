package;


/**
 * The fragment operator tree keeps all fragment operators.
 * 
 * All fragment operators are stored in an defined order that is derived from 
 * their pathes. Fragment operators are applied in order of their pathes, from
 * button to top.
 *
 * Fragment operators also add ekevatuib cacge ioerators in the tree using the samse id string.
 * That way, they also are sorted and executed at the same level.
 */
class FragmentOperatorTree extends Entity
{
    private var _mapFOs: Map<String, IFragmentOperator>;
    private var _keysFOs: Array<String>;
    private var _worldMetaGen: WorldMetaGen;

    public function apply( f: IFragmentOperator -> Void): Void
    {
        for(path in _keysFOs) {
            var fo: IFragmentOperator = _mapFOs[path];
            f(fo);
        }
    }


    /**
     * Remove an existing fragment operator from this world.-
     *
     * TODO: Look, wether we also had an elevation operator associated with
     * this fragment operator. If we had, remove it from the world.
     */
    public function removeFragmentOperator(fo: IFragmentOperator): Void
    {
        var path = fo.fragmentOperatorGetPath();
        if (!_mapFOs.exists(path)) {
            throw 'FragmentOperatorTree.removeFragmentOperator(): path "$path" does not exist.';
        }
        _mapFOs.remove(path);
        _keysFOs.remove(path);
    }


    /**
     * Add another fragment operator to this world.
     * 
     * TODO: Somehow ask the fragment operator for an elevation operator.
     * Add that elevation operator to the elevation cache.
     */
    public function addFragmentOperator(fo: IFragmentOperator): Void
    {
        var path = fo.fragmentOperatorGetPath();
        if (_mapFOs.exists(path)) {
            throw 'FragmentOperatorTree.addFragmentOperator(): path "$path" already exists.';
        }
        _mapFOs.set(path, fo);
        _keysFOs.push(path);
        _keysFOs.sort(function(a,b): Int {
            if( a<b ) {
                return -1;
            } else {
                if( a>b ) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });
    }

    public function new (worldMetaGen: WorldMetaGen)
    {
        _worldMetaGen = worldMetaGen;
        WorldMetaGen.cat.catAddGlobalEntity('FragmentOperatorTree', this);
        _mapFOs = new Map<String, IFragmentOperator>();
        _keysFOs = new Array<String>();
    }
}