
/**
 * Random seed generator for all kinds of objects,
 */
class RandomSource {
    private var _seed0:String;
    private var _randomSeed:Int;

    public function getFloat():Float
    {
        _randomSeed = (_randomSeed * 16807) % 2147483647;
        while( _randomSeed<0 ) _randomSeed += 2147483647;
        //        trace('_random is $_randomSeed');
        return _randomSeed / 2147483647.0;
    }


    public function clear () : Void
    {
        var l:Int = _seed0.length;
        _randomSeed = 0;
        for( i in 0...l ) {
            _randomSeed += _seed0.charCodeAt( i )*(i*147) + 1065434;
        }
    }

    public function new ( str:String )
    {
        _seed0 = str;
        clear();
    }

}