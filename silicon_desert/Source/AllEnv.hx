package;

import away3d.cameras.*;
import away3d.containers.*;
import away3d.controllers.*;
import away3d.debug.*;
import away3d.entities.Mesh;
import away3d.extrusions.*;
// import away3d.filters.BloomFilter3D;
// import away3d.filters.DepthOfFieldFilter3D;
import away3d.lights.*;
import away3d.materials.*;
import away3d.materials.lightpickers.*;
import away3d.materials.methods.*;
import away3d.primitives.*;
import away3d.textures.*;
import away3d.utils.*;

import openfl.display.*;
import openfl.events.*;
import openfl.filters.*;
import openfl.text.*;
import openfl.ui.*;


/**
 * Contains the 3d object environment in which the fragments are built.
 *
 * This defines parameters relevant for all object generation functions.
 *
 * Currently it contains
 * - global objects, such as light or skybox
 * - common textures
 * - it holds and creates the current interface.
 *
 * So it is the world metagen that contain the properties and the operators that
 * create the world.
 * 
 * The terrain env is the canvas that can be used to crate the actual Terrain Environments
 *
 */
class AllEnv
{
    //light objects
    private var _sunLight:DirectionalLight;
    private var _lightPicker:StaticLightPicker;
    private var _fogMethod:FogMethod;
    
    //material objects
    private var _skyTexture:BitmapCubeTexture;
    private var _skyBox:SkyBox;

    private var _scene:Scene3D;

    public var mapRenderer(default,null):MapRenderer;

    public var worldMetaGen(default,null):WorldMetaGen;
    public var worldLoader(default,null):WorldLoader;

    /**
     * Initialise the lights
     */
    private function initLights():Void
    {
        _sunLight = new DirectionalLight(
            -80*WorldMetaGen.meter, 
            -12*WorldMetaGen.meter,
            -200*WorldMetaGen.meter);
        _sunLight.color = 0xfffdc5;
        _sunLight.ambient = 1;
        
        _lightPicker = new StaticLightPicker( [_sunLight] );
        
        //create a global fog method
        _fogMethod = new FogMethod( 
            50*WorldMetaGen.meter,
            2000*WorldMetaGen.meter, 0x303060 );
    }
 

    public function allEnvironmentUnload()
    {
        _skyBox = null;
        _skyTexture= null;
    }


    public function allEnvironmentLoadStatic()
    {
        initLights();

        _skyTexture = new BitmapCubeTexture(
            Cast.bitmapData("bluesky/skyleft.jpg"), Cast.bitmapData("bluesky/skyright.jpg"),
            Cast.bitmapData("bluesky/skybottom.jpg"), Cast.bitmapData("bluesky/skytop.jpg"),
            Cast.bitmapData("bluesky/skyback.jpg"), Cast.bitmapData("bluesky/skyfront.jpg")
        );

        _skyBox = new SkyBox( _skyTexture );
    }


    public function allEnvironmentRemove( scene:Scene3D )
    {
        _scene.removeChild( _skyBox );
        _scene.removeChild( _sunLight );
        _scene = null;
    }


    public function allEnvironmentAdd( scene:Scene3D, detail:Float )
    {
        _scene = scene;
        _scene.addChild( _sunLight );
        _scene.addChild( _skyBox );
    }


    public function allEnvironmentGetLightPicker():StaticLightPicker
    {
        return _lightPicker;
    }


    public function allEnvironmentGetFogMethod():FogMethod
    {
        return _fogMethod;
    }

    public function new (
        worldMetaGen0:WorldMetaGen,
        worldLoader0:WorldLoader
    ) {
        worldMetaGen = worldMetaGen0;
        worldLoader = worldLoader0;
        mapRenderer = new MapRenderer(this);
        _scene = null;
    }        

}