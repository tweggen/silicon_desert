
package;

interface EntityOwner {
    public entityOwnerAdopt(newChild: Entity);

    public entityOwnerRelease(oldChild: Entity);
}