
package;

import openfl.geom.Vector3D;

interface IFragmentOperator {

    /**
     * Return the path describing the order of the operator.
     * This is a preliminary operator until we found some generic way
     * to describe the tree-like structure the operators live in.
     *
     * The order of the fragment operators is determined by the order
     * of their pathes.
     * 
     * The pathes also are used as keys for the various internal
     * pipelines.
     */
    public function fragmentOperatorGetPath(): String;

    /**
     * Return the bounding box of this particular operator instance.
     * Operators that do not intersect with the fragment(s) in question
     * probably are not called.
     */    
    public function fragmentOperatorGetBoundingBox(
        allEnv: AllEnv,
        worldFragment: WorldFragment
    ) : Array<Vector3D>;

    /*
     * Filter terrain for this fragment operator.
     * The function may modify terrain within the boundaries given by this
     * operator's boundary box.
     *
     * It can read any parts of the terrain7tt77 in the state before its filter
     * has been applied.
     * 
     * The terrain filter function can be called at different times than the
     * terrain operator. In fact, it even can be called without the terrain
     * being loaded.
     */
    // public function fragmentOperatorFilterElevataion

    /**
     * Apply this operator to a world fragment.
     *
     * This operator shall be stateless. That means, for a certain world
     * fragment in a given state and a certain configuratino, it shall generate
     * the same output.
     */
    public function fragmentOperatorApply(
        allEnv: AllEnv,
        worldFragment: WorldFragment) : Void;

}
