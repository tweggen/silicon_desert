
/** 
 * Describes a generic terrain ground.
 *
 * The ground operator applies the seeded ground to the given terrain element.
 * To accelerate ground generatioin, it keeps an skeleton grid down to terrain
 * fragment size.
 */
class GroundOperator extends Entity
{
    var _maxWidth:Float;
    var _maxHeight:Float;
    var _skeletonWidth:Int;
    var _skeletonHeight:Int;

    var _skeletonElevations:Array<Array<Float>>;

    private var _rnd:RandomSource;
    private var _worldMetaGen:WorldMetaGen;


    /** 
     * Create the elevation skeleton for the map. Sub-terrains may further refine this elevation mesh.
     */
    private function createSkeleton()
    {
        _rnd.clear();

        _skeletonElevations = [for (y in 0..._skeletonHeight) [for (x in 0..._skeletonWidth) 0]];

        /*
         * Now recursively generate.
         * First generate corner heights, then recursively refine.
         */
        var x0:Int = 0;
        var x1:Int = _skeletonWidth-1;
        var y0:Int = 0;
        var y1:Int = _skeletonHeight-1;

        var amplitude:Float = _worldMetaGen.maxElevation - _worldMetaGen.minElevation;
        var bias:Float = _worldMetaGen.minElevation;
        _skeletonElevations[y0][x0] = _rnd.getFloat() * (amplitude)+bias;
        _skeletonElevations[y0][x1] = _rnd.getFloat() * (amplitude)+bias;
        _skeletonElevations[y1][x0] = _rnd.getFloat() * (amplitude)+bias;
        _skeletonElevations[y1][x1] = _rnd.getFloat() * (amplitude)+bias;

        // This was the start, now refine.
        elevation.Tools.refineSkeletonElevation(
            0, 0,
            _skeletonElevations,
            _worldMetaGen.minElevation, 
            _worldMetaGen.maxElevation,
            x0, y0, x1, y1 );
    }

    public function getSkeleton(): Array<Array<Float>> {
        return _skeletonElevations;
    }

    /**
     * Create the ground operator.
     *
     * Immediately after creation, it will compute a ground skeleton.
     */
    public function new (
        worldMetaGen0:WorldMetaGen,
        seed0:String )
    {
        _worldMetaGen = worldMetaGen0;

        _maxWidth = _worldMetaGen.maxWidth;
        _maxHeight = _worldMetaGen.maxHeight;

        var fragmentSize = WorldMetaGen.fragmentSize;

        _skeletonWidth = Std.int( (_maxWidth+fragmentSize-1)/fragmentSize ) + 1;
        _skeletonHeight = Std.int( (_maxHeight+fragmentSize-1)/fragmentSize ) + 1;

        _rnd = new RandomSource( seed0 );
 
        createSkeleton();

        WorldMetaGen.cat.catAddGlobalEntity('GroundOperator', this);
    }

}
