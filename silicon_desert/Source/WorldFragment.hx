package;

import away3d.cameras.*;
import away3d.containers.*;
import away3d.controllers.*;
import away3d.core.base.Geometry;
import away3d.core.base.SubGeometry;
import away3d.debug.*;
import away3d.entities.Mesh;
import away3d.entities.SegmentSet;
import away3d.extrusions.*;
import away3d.filters.BloomFilter3D;
import away3d.filters.DepthOfFieldFilter3D;
import away3d.lights.*;
import away3d.materials.*;
import away3d.materials.lightpickers.*;
import away3d.materials.methods.*;
import away3d.primitives.*;
import away3d.textures.*;
import away3d.utils.*;

import openfl.display.*;
import openfl.events.*;
import openfl.filters.*;
import openfl.text.*;
import openfl.ui.*;

import openfl.Vector;
import openfl.geom.Vector3D;


/**
 * Contains a instatiated fragment of the real world.
 * This fragment contains the actual away3d objects as well as the referemced
 * operators.
 *
 * This includes information about the real world's elevation at that point.
 * It references things and floormaps.
 * 
 * The world fragments are instantiated by the world loader.
 *
 * After instantiation, a world fragment is an empty canvas: It contains
 * an empty terrain, no objects and no characters populating the fragment.
 *
 * A world fragment is free to use global objects for the engine from
 * terrain3denv.
 *
 * Information about the properties of the world is obtained from WorldMetaGen. 
 */
//@:build(Profiler.buildAll())

class WorldFragment 
{
    private var _allEnv:AllEnv;

    private var _myKey:String;

    public var lastIteration:Int = 0;
    private var _scene:Scene3D;

    private var _currentDetail:Float;

    private var _matTerrain:TextureMaterial;

    private var _o3dContainer:ObjectContainer3D;

    private var _o3dTerrain:Elevation;

    private var _o3dGround:Mesh;

    private var _listPlaceObjects:List<PlaceObject>;

    private var _listCharacters:List<ICharacter>;

    private var _rnd:RandomSource;

    public var x(default,null):Float;
    public var y(default,null):Float;
    public var z(default,null):Float;

    public var i(default,null):Int;
    public var j(default,null):Int;
    public var k(default,null):Int;

    /**
     * Our array of elevations.
     */
    public var _elevations:Array<Array<Float>> = null;

    private var _groundResolution: Int = 0;
    private var _groundNElevations: Int = 0;


    /**
     * Preliminary function to let operators add objects directly.
     */
    // public function worldFragmentGetO3dContainer() : ObjectContainer3D {
    //     return _o3dContainer;
    //}

    public function addPlaceObject(placeObject: PlaceObject) : Void {
        _listPlaceObjects.add(placeObject);
        placeObject.thingAdd(_o3dContainer);
    }

    public function addMesh(mesh: away3d.entities.Mesh) : Void {
        _o3dContainer.addChild(mesh);
    }

    /**
     * Test, wether the given world coordinate is inside the cluster.
     */
    public function isInsideLocal( x: Float, z: Float ): Bool {

        var fsh: Float = WorldMetaGen.fragmentSize / 2.0;            
        if (
            (x)>=(fsh)
            || (x)<(-fsh)
            || (z)>=(fsh)
            || (z)<(-fsh)
        ) {
            return false;
        } else {
            return true;
        }

    }
    public function isInside( x0: Float, z0: Float ): Bool {
        var cx:Float = x0 - x;
        var cz:Float = z0 - z;
        return isInsideLocal( cx, cz );
    }

    /**
     * For performance reasons, we directly receive a ground array. Terrain operators
     * can set the array for this fragment.
     *
     * Clipping is applied so that only parts of the array are used that are 
     * meaningful for this fragment.
     *
     * @param ax
     *     Left index in the array
     * @param ay
     *     Top index in the array
     * @param bx
     *     Rightmost index in the array.
     * @param by
     *     Bottom index in the array
     * @param dx
     *     left index in fragment elevation
     * @param dy
     *     top index in fragment elevation
     */
    public function worldFragmentSetGroundArray(
        groundArray:Array<Array<Float>>,
        groundResolution: Int,
        ax: Int, ay: Int, bx: Int, by: Int,
        dx: Int, dy: Int )
    {
        if( groundResolution != _groundResolution ) {
            throw 'worldFragmentSetGroundArray(): Inconsistent groundResolution';
        }

        /*
         * Compute my maximum bottom right.
         */
        var mx: Int = groundResolution;
        var my: Int = groundResolution;

        /*
         * Sort the source, adjusting destination.
         */
        if( bx<ax ) {
            var h = ax; ax = bx; bx = h;
            dx -= bx-ax;
        }
        if( by<ay ) {
            var h = ay; ay = by; by = h;
            dy -= by-ay;
        }

        /*
         * Totally out of range?
         */
        if( ax>mx || ay>my || bx<0 || by<0 ) {
            /*
             * Does not intersect with this fragment.
             */
            return;
        }

        /* 
         * Clip the values with my top left.
         */
        if( dx<0 ) {
            ax = ax - dx;
            dx = 0;
        }
        if( dy<0 ) {
            ay = ay - dy;
            dy = 0;
        }

        /*
         * Compute destination bottom right.
         */
        var ex = dx + (bx-ax);
        var ey = dy + (by-ay);

        /*
         * Clip with my bottom right.
         */
        if( ex>mx ) {
            bx = bx - (ex-mx);
            ex = mx;
        }
        if( ey>my ) {
            by = by - (ex-my);
            ey = my;
        }

        /*
         * Now, we can loop.
         */
        var y = 0, ymax=(ey-dy);
        while(y<=ymax) {
            var x = 0, xmax=(ex-dx);
            while(x<=xmax) {
                _elevations[dy+y][dx+x] = groundArray[ay+y][ax+x];
                ++x;
            }
            ++y;
        }
    }


    private function createGroundWithResolution(coarseness: Int):Mesh
    {
        var geometry:Geometry = new Geometry();

        var subGeometry:SubGeometry = new SubGeometry();
        subGeometry.autoDeriveVertexNormals = true;
        subGeometry.autoDeriveVertexTangents = true;
        geometry.addSubGeometry( subGeometry );

        var coarseResolution: Int = Std.int(_groundResolution / coarseness);
        var coarseNElevations: Int = coarseResolution + 1;
        // trace('_groundResolution: '+_groundResolution+", coarseResolution: "+coarseResolution);

        var verts:Vector<Float> = new Vector<Float>();
        for( iterY in 0...coarseNElevations ) {
            var y0: Int = Std.int(((iterY+0) * _groundResolution) / coarseResolution);
            var y1: Int = Std.int(((iterY+1) * _groundResolution) / coarseResolution);
            for( iterX in 0...coarseNElevations ) {
                var localX:Float = -WorldMetaGen.fragmentSize/2.0 
                    + (WorldMetaGen.fragmentSize*iterX/coarseResolution);
                var localZ:Float = -WorldMetaGen.fragmentSize/2.0
                    + (WorldMetaGen.fragmentSize*(iterY)/coarseResolution);
                var x0: Int = Std.int(((iterX+0) * _groundResolution) / coarseResolution);
                var x1: Int = Std.int(((iterX+1) * _groundResolution) / coarseResolution);
                // trace("iterX: "+iterX+", x0: "+x0+", x1: "+x1);

                var localY:Float = 0.;

                if(1==coarseness) {
                    localY = _elevations[iterY][iterX];
                } else {
                    var n: Int = 0;
                    for( ey in y0...y1 ) {
                        for( ex in x0...x1 ) {
                            // trace("ex: "+ex+", ey="+ey+", localY="+localY);
                            localY += _elevations[ey][ex];
                            ++n;
                        }
                    }
                    localY = localY / n;
                }
                if( localY>-0.00001 && localY <0.00001 ) {
                    // trace( "Warning: elevation ca. 0 at "+iterX+", "+iterY );
                }
                verts.push( localX );
                verts.push( localY );
                verts.push( localZ );
                //trace( "x: "+localX+", y: "+localY+", z: "+localZ );
            }
        }

        var texWidth:Float = 25.;
        var texHeight:Float = 25.;

        var uvs:Vector<Float> = new Vector<Float>();
        for( iterY in 0...coarseNElevations ) {
            for( iterX in 0...coarseNElevations ) {
                var localU:Float = 
                    (iterX*WorldMetaGen.fragmentSize)
                        /coarseResolution/texWidth;
                // module in range 0..1
                localU = localU-Math.ffloor(localU);
                var localV:Float = 
                    (iterY*WorldMetaGen.fragmentSize)
                        /coarseResolution/texHeight;
                localV = localV-Math.ffloor(localV);
                uvs.push( localU );
                uvs.push( localV );
            }
        }


        var indices:Vector<UInt> = new Vector<UInt>();
        var w:Int = coarseNElevations;
        for( iterY in 0...coarseResolution ) {
            for( iterX in 0...coarseResolution ) {
                indices.push( (iterY+1)*w + iterX );
                indices.push( iterY * w + iterX + 1 );
                indices.push( iterY * w + iterX );

                indices.push( (iterY+1)  *w + iterX );
                indices.push( (iterY+1) * w + iterX + 1 );
                indices.push( iterY  * w + iterX + 1 );
            }
        }

        subGeometry.updateVertexData( verts );
        subGeometry.updateIndexData( indices );
        subGeometry.updateUVData( uvs );

        var mesh:Mesh = new Mesh( 
            geometry,
            _matTerrain );

        return mesh;
    }


    private function createGround(): Mesh
    {
        return createGroundWithResolution(1);
    }


    public function worldFragmentUnload():Int
    {
        for( placeObject in _listPlaceObjects ) {
            placeObject.thingRemove( _o3dContainer );
            placeObject.thingUnload();
        }
        _listPlaceObjects.clear();
        _listPlaceObjects = null;
        _matTerrain = null;
        _allEnv = null;
        return 0;
    }


    public function worldFragmentLoadGround():Int
    {
        // Re-seed to what we are.
        _rnd.clear();

        // TXWTODO: Why not via factory in metagen?
        if( _matTerrain==null ) {
            _matTerrain = new TextureMaterial( 
                Cast.bitmapTexture( "terrain/gridlines1.jpg" ) );
            _matTerrain.lightPicker = _allEnv.allEnvironmentGetLightPicker();
            _matTerrain.ambientColor = 0xffffff;
            _matTerrain.ambient = 1;
            _matTerrain.specular = .2;
            _matTerrain.repeat = true;
            _matTerrain.addMethod( _allEnv.allEnvironmentGetFogMethod() );
        }

        _o3dContainer = new ObjectContainer3D();

        /*
         * load the ground if we have any.
         */
        _o3dGround = createGround();
        _o3dGround.x = 0;
        _o3dGround.y = 0;
        _o3dGround.z = 0;
        _o3dContainer.addChild( _o3dGround );

        return 0;
    }

    /** 
     * Load static objects.
     * 
     * TXWTODO: The purpose of this function yet is unclear: We should
     * have another operator creating entities as clusters or buildings/streets
     * inside this clusters, having operators add the actual objects instead of
     * creating them in-place here.
     */
    private function loadStaticObjects():Int
    {
        _rnd.clear();
     
        return 0;
    }


    private function loadStaticMaterials():Int
    {
        return 0;
    }


    public function worldFragmentLoadStatic( envCurrent:AllEnv ):Int
    {
        _allEnv = envCurrent;

        _listPlaceObjects = new List<PlaceObject>();
        _listCharacters = new List<ICharacter>();

        loadStaticMaterials();

        loadStaticObjects();
        return 0;
    }


    /**
     * Spawn all characters required for this fragment.
     */
    public function worldFragmentPopulate():Int
    {
        return 0;
    }


    public function worldFragmentRemove( scene:Scene3D )
    {
        if( null == _scene ) {
            trace( "Already removed from scene." );
            return -1;
        }

        // We don't remove the actual objects.
        scene.removeChild( _o3dContainer );

        return 0;
    }


    public function worldFragmentAdd( 
        scene:Scene3D, 
        newDetail:Float ) : Int
    {
        if( null != _scene ) {
            trace( "Already added to scene." );
            return -1;
        }
        if( null == _o3dGround ) {
            trace( "Not loaded yet.\n" );
            return -1;
        }
        _scene = scene;

        _scene.addChild( _o3dContainer );
        _o3dContainer.x = x;
        _o3dContainer.y = y;
        _o3dContainer.z = z;
        // trace( "Added child to "+_o3dContainer.x+", "+_o3dContainer.z );

        return 0;
    }


    public function worldFragmentBehaveStatic() : Int
    {
        return 0;
    }


    public function worldFragmentBehave() : Int
    {
        if( null == _listCharacters ) {
            throw "WorldFragment.worldFragmentBehave(): _listCharacters is null";
        }
        for( character in _listCharacters ) {
            character.characterBehave();
            // TXWTODO: Ask for removeMe
            var pos = character.characterGetWorldPos();
            var mesh = character.characterGetMesh();
            // TXWTODO: Test for changed mesh.
            // trace('pos = ${pos.x}, ${pos.y}, ${pos.z}, mypos is $x, $y, $z .');
            mesh.x = pos.x - x;
            mesh.y = pos.y - y;
            mesh.z = pos.z - z;
        }

        return 0;
    }


    /*
     * Directly set the terrain elevation in elevation resolution size.
     */
    public function worldFragmentSetElevation( 
        x0:Int, y0:Int, elevation:Float ) : Void
    {
        if( x0<0 || x0>=_groundNElevations ) {
            trace( "Invalid x0: "+x0 );
            return;
        }
        if( y0<0 || y0>=_groundNElevations ) {
            trace( "Invalid y0: "+y0 );
            return;
        }
        _elevations[y0][x0] = elevation;
    }


    /**
     * Create an array capable of holding the elevation data
     * of the given resolution.
     */
    private function createElevationArray()
    {
        var plusone:Int = _groundNElevations+1;
        _elevations = [for (x in 0...plusone) [for (y in 0...plusone) 0]];
    }

    public function getId(): String {
        return _myKey;
    }


    public function worldFragmentAddCharacter( character: ICharacter ): Void {
        _listCharacters.add( character );
        // TXWTODO: Copy from behave
        {
            character.characterBehave();
            // TXWTODO: Ask for removeMe
            var pos = character.characterGetWorldPos();
            var mesh = character.characterGetMesh();
            // TXWTODO: Test for changed mesh.
            mesh.x = pos.x - x;
            mesh.y = pos.y - y;
            mesh.z = pos.z - z;
            _o3dContainer.addChild(mesh);
        }
    }


    public function new (
        terrainEnv:AllEnv,
        strKey:String,
        i0:Int,
        j0:Int,
        k0:Int,
        xOffset:Float,
        yOffset:Float,
        zOffset:Float )
    {
        _allEnv = null;
        _allEnv = terrainEnv;
        _listCharacters = null;
        _listPlaceObjects = null;
        _groundResolution = WorldMetaGen.groundResolution;
        _groundNElevations = _groundResolution + 1;
        _myKey = strKey;
        _rnd = new RandomSource( _myKey );
        _currentDetail = 0.0;
        _scene = null;
        _o3dTerrain = null;
        _matTerrain = null;
        x = xOffset;
        y = yOffset;
        z = zOffset;
        i = i0;
        j = j0;
        k = k0;

        // Create an initial elevation array that still is zeroed.
        createElevationArray();
    }
}