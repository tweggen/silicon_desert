package streets;


/**
 * Given a stroke store containing one or more street networks,
 * create the areas between the streets for further processing.
 *
 * Note: We could create a navmesh as well from that, however, we prefer 
 * to create it from the actual buildings in there. Also the quarters
 * might be navigable in the end.
 */
class QuarterGenerator {

    private var _strokeStore: StrokeStore;
    private var _quarterStore: QuarterStore;
    private var _traceGenerate = false;

    private var _rnd: RandomSource;

    static private function anorm(a: Float): Float {
        while(a<-Math.PI) {
            a += Math.PI * 2.0;
        }
        while(a>Math.PI) {
            a -= Math.PI * 2.0;
        }
        return a;
    }

    /**
     * Generate quarters by following the strokes.
     *
     * - Mark all strokes untraversed in either direction.
     * - Iterate through all street points.
     + - for every stroke (starting and ending) at this endpoint, follow it
     *   to create a quarter, unless it already has been traversed. Mark it
     *   traversed afterwards.
     */
    public function generate(): Void {

        _strokeStore.clearTraversed();
        var points = _strokeStore.getStreetPoints();
        for( spStart in points ) {
            if( _traceGenerate ) trace('QuarterGenerator(): Tracing Point ${spStart.pos.x}, ${spStart.pos.y}');
            var angleStrokes = spStart.getAngleArray();
            for( stroke in angleStrokes ) {
                var spDest = null;
                // Which direction?
                var isAlreadyTraversed = false;
                if(stroke.a == spStart) {
                    if(stroke.traversedAB) {
                        isAlreadyTraversed = true;
                    }
                    spDest = stroke.b;
                } else if(stroke.b == spStart) {
                    if(stroke.traversedBA) {
                        isAlreadyTraversed = true;
                    }
                    spDest = stroke.a;
                } else {
                    throw 'QuarterGenerator: Invalid stroke encountered.';
                }
                if(spStart==spDest) {
                    throw 'QuaterGenerator: Invalid stroke: Start and end is the same.';
                }

                if(isAlreadyTraversed) {
                    continue;
                }
                if( _traceGenerate ) trace('QuarterGenerator():');
                if( _traceGenerate ) trace('QuarterGenerator(): Starting with stroke from ${spStart.pos.x}, ${spStart.pos.y} to ${spDest.pos.x}, ${spDest.pos.y}');

                /*
                 * We know that we need to start from spStart using "stroke". Follow the loop.
                 */
                var spCurr = spStart;
                var strokeCurrent = stroke;
                var solestroke = false;

                var quarter = new Quarter();

                while(true) {
                    
                    if(null==strokeCurrent) {
                        throw 'QuarterGenerator(): strokeCurrent is null';
                    }
                    if(null==spCurr) {
                        throw 'QuarterGenerator(): spCurr is null';
                    }

                    /*
                     * For readability: First figure out the direction.
                     */
                    var isAB = false;
                    var spNext = null;
                    if(strokeCurrent.a==spCurr) {
                        isAB = true;
                        spNext = strokeCurrent.b;
                    } else if(strokeCurrent.b==spCurr) {
                        isAB = false;
                        spNext = strokeCurrent.a;
                    } else {
                        throw 'QuarterGenerator: Invalid stroke following quarter.';
                    }

                    /*
                     * That's a bit difficult: stroke.angle is valid from a to b. So if we 
                     * try a stroke while being point a, everything is fine.
                     * However, if we are b, we need to invert the angle for the purpose of
                     * following.
                     */
                    var followAngle = anorm( strokeCurrent.angle + ((!isAB)?Math.PI:0.) );

                    /*
                     * Hint what we are doing.
                     */
                    if( _traceGenerate ) trace('QuarterGenerator(): Following angle ${followAngle} (${anorm(followAngle+Math.PI)}) from ${isAB?"A":"B"} to ${isAB?"B":"A"}: ${spNext.pos.x}, ${spNext.pos.y}');

                    /*
                     * In every iteration: Follow strokeCurrent from spCurr.
                     * If spCurr is spStart, terminate (case 1).
                     * Look for the next stroke clockwise to strokeCurrent.
                     * If it is strokeCurrent, terminate (case 2).
                     * add the new spCurr to the Quarter.
                     * repeat.
                     */
                    if(isAB) {
                       if(strokeCurrent.traversedAB) {
                            throw 'QuarterGenerator(): Attempt to traverseAB twice';
                        }
                        strokeCurrent.traversedAB = true;
                    } else /* B to A */ {
                        if(strokeCurrent.traversedBA) {
                            throw 'QuarterGenerator(): Attempt to traverseBA twice';
                        }
                        strokeCurrent.traversedBA = true;
                    }

                    var quarterDelim = new QuarterDelim();
                    quarterDelim.streetPoint = spCurr;
                    quarterDelim.stroke = strokeCurrent;
                    quarter.addQuarterDelim(quarterDelim);

                    if(spNext==spStart) {
                        if( _traceGenerate ) trace('QuarterGenerator(): Reached start again.');
                        break;
                    }

                    var strokeNext = spNext.getNextAngle(strokeCurrent, followAngle, true);
                    if(null==strokeNext || strokeNext==strokeCurrent) {
                        if( _traceGenerate ) trace('QuarterGenerator(): Followed same stroke back because there is no other angle.');
                        /*
                         * So follow myself back in the other direction.
                         * That means, spCurr (regularily) will become spNext.
                         * The next stroke, however, will be the same one as the current one.
                         */
                        strokeNext = strokeCurrent;
                    }

                    /*
                     * Iterate to the next one.
                     */
                    strokeCurrent = strokeNext;
                    spCurr = spNext;
                }

                if(solestroke) {
                    if( _traceGenerate ) trace('QuarterGenerator(): Leaving loop because this is a single stroke.');
                } else {
                    _quarterStore.add( quarter );
                }
            }
        }

    }

    public function reset(
        seed0: String,
        quarterStore: QuarterStore,
        strokeStore: StrokeStore
    ) {
        _rnd = new RandomSource(seed0);
        _quarterStore = quarterStore;
        _strokeStore = strokeStore;
    }

    public function new () {
    }  
}
