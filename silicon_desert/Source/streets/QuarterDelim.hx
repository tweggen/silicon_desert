package streets;


/**
 * Every quarter is surrounded by a lot of quarterDelims.
 * They provide the semantic connection to all the streets.
 */
class QuarterDelim {
    public var streetPoint: StreetPoint;
    public var stroke: Stroke;

    public function new () {
        streetPoint = null;
        stroke = null;
    }
}