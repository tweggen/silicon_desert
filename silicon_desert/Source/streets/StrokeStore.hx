package streets;

import streets.StrokeIntersection;
import geom.Point;

class StrokeStore {
    private var _listStrokes: List<Stroke>;
    private var _listPoints: List<StreetPoint>;

    private var _traceStrokes = false;

    /**
     * Remove the given stroke
     */
    public function remove(p: Stroke) {
        if( null == p.store ) {
            throw 'StrokeStore: Stroke not in any store.';
        }
        if( this != p.store ) {
            throw 'StrokeStore: Stroke not in this store.';
        }

        _listStrokes.remove(p);
        p.store = null;
        p.a.removeStartingStroke(p);
        p.b.removeEndingStroke(p);
    }


    public function intersectsMayTouch(cand: Stroke): StrokeIntersection {
        var ax = cand.a.pos.x;
        var ay = cand.a.pos.y;
        var bx = cand.b.pos.x;
        var by = cand.b.pos.y;

        for (stroke in _listStrokes) {
            var si = stroke.intersects(cand);
            // Default to collide.
            var xx = true;
            if(null==si) {
                xx = false;
            } else {
                // We have a collision. But is this probably just the end.
                var dxa: Float;
                var dya: Float;
                var dxb: Float;
                var dyb: Float;
                dxa = ax-si.pos.x;
                dya = ay-si.pos.y;
                dxb = bx-si.pos.x;
                dyb = by-si.pos.y;
                if( (dxa*dxa+dya*dya) < 0.0001 
                    || (dxb*dxb+dyb*dyb) < 0.001 ) {
                    xx = false;
                }
            }
            if(xx) {
                //trace('StrokeStore: ${cand.toString()} intersects ${stroke.toString()}');
                return si;
            }
        }
        return null;
    }


    public function findCloserThan( 
        sp0: StreetPoint,
        minDist: Float,
        spNot: StreetPoint
    ): StreetPoint {
        var minDist2 = minDist*minDist;
        for( sp in _listPoints ) {
            if( sp0 != sp && spNot != sp ) {
                var dx = (sp0.pos.x-sp.pos.x);
                var dy = (sp0.pos.y-sp.pos.y);
                if (minDist2 > dx*dx+dy*dy) {
                    return sp;
                }
            }
        }
        return null;
    }


    /**
     * Return the point that is closest to the given stroke.
     */
    public function getClosestPoint(
        stroke: Stroke): StrokeIntersection
    {
        if( _traceStrokes ) trace('getClosestPoint(): Testing stroke ${stroke.toString()} ');
        var closestDist = 1000000000000.;
        var closestPoint: StreetPoint = null;
        var closestCrossproduct = 0.0;

        /*
         * Note: We use the dot product of the vectors to compute the 
         * distance. Let's say AB is our stroke and C is our point.
         * then p = AB dot AC is |AB|*|AC|*cos(ABvAC) .
         * Dividing this by |AB| gives |AC|*cos(ABvAC) which incidentally
         * is the closest distance from C to AB.
         * However, we also need to know whether that point is close to 
         * something within this stroke. So we use pythagoras to compute
         * the length A to the projection of C to AB.
         */
        var abx = stroke.b.pos.x - stroke.a.pos.x;
        var aby = stroke.b.pos.y - stroke.a.pos.y;
        for (sp0 in _listPoints) {
            /*
             * Skip stroke's end points.
             */
            if( sp0==stroke.a || sp0==stroke.b ) {
                // trace( 'Skipping point ${sp0.pos.x}, ${sp0.pos.y}, because its part of this stroke.');
                continue;
            }
            var acx = sp0.pos.x - stroke.a.pos.x;
            var acy = sp0.pos.y - stroke.a.pos.y;
            var dotproduct = abx * acx + aby * acy;
            /*
             * If the dot product is negative, the point is out of range anyway.
             */
            if( dotproduct < 0. ) {
                // trace( 'Skipping point ${sp0.pos.x}, ${sp0.pos.y}, because its on the wrong side.');
                continue;
            }
            var crossproduct = abx * acy - aby * acx;
            var dist = Math.abs(crossproduct) / stroke.length;
            // trace( 'dist=$absdist');
            if( dist<closestDist ) {
                closestDist = dist;
                closestPoint = sp0;
                closestCrossproduct = crossproduct;
            }
        }

        if( null != closestPoint ) {
            /*
             * Compute the distance between A and the projection of C on AB.
             */
            var acx = closestPoint.pos.x - stroke.a.pos.x;
            var acy = closestPoint.pos.y - stroke.a.pos.y;
            var ac2 = acx*acx + acy*acy;
            var ad2 = ac2 - closestDist*closestDist;
            var ad = Math.sqrt( ad2 );

            if( ad<=stroke.length ) {
                var si = new StrokeIntersection();
                si.pos = closestPoint.pos;
                si.streetPoint = closestPoint;
                si.strokeExists = stroke;
                si.scaleExists = closestDist;
                // trace( 'Stroke in range ($ad) for ${si.pos.x}, ${si.pos.y}, length ${stroke.length} distance $closestDist' );
                return si;
            } else {
                // trace( 'Stroke out of range ($ad).' );
            }
        } else {
        }
        return null;
    }


    public function add(stroke: Stroke) {

        if( stroke.store != null ) {
            if( stroke.store == this ) {
                throw 'StrokeStore: Stroke already in this store.';
            } else {
                throw 'StrokeStore: Stroke already in other store.';                
            }
        }
        if( !stroke.a.inStore ) {
            stroke.a.inStore = true;
            _listPoints.add( stroke.a );
        }
        stroke.a.addStartingStroke( stroke );
        if ( !stroke.b.inStore ) {
            stroke.b.inStore = true;
            _listPoints.add( stroke.b );
        }
        stroke.b.addEndingStroke( stroke );

        stroke.store = this;
        _listStrokes.add( stroke );
    }


    public function areConnected(sp0: StreetPoint, sp1: StreetPoint ) : Bool
    {
        for(stroke in _listStrokes) {
            if( stroke.a == sp0 && stroke.b == sp1
                || stroke.b == sp0 && stroke.a  == sp1) {
                if( _traceStrokes ) trace('StrokeStore(): Already connected in ${stroke.toString()}.');
                return true;
            }
        }
        return false;
    }


#if 0
    /**
     * Starting from the given streetpoint, navigate to the next stroke.
     */
    public function getNextStroke(
        sp0: StreetPoint,
        str0: Stroke
    ) {
        var spNew: StreetPoint = null;
        if( str0.a == sp0 ) {
            spNew = str0.b;
        } else if( str0.b == sp0 ) {
            spNew = str0.a;
        } else {
            throw 'StrokeStore.getNextStroke(): Called with streetpoint/stroke pair that does not match.';
        }
        var angleOld = str0.angle;

        /*
         * Now, for a clockwise loop, we need to find the stroke
         * starting/ending in spNew with the smallest angle greater than
         * my angle. In that case, it does not matter whether this is a
         * starting or an ending stroke.
         */
        var strNew = spNew.getNextAngle( str0, angleOld, true /* clockwise */ );
    }
#end


    public function getStrokes(): List<Stroke> {
        return _listStrokes;
    }


    public function getStreetPoints(): List<StreetPoint> {
        return _listPoints;
    }


    public function clearTraversed() {
        for( stroke in _listStrokes ) {
            stroke.traversedAB = false;
            stroke.traversedBA = false;
        }
    }


    public function new () {
        _listStrokes = new List<Stroke>();
        _listPoints = new List<StreetPoint>();
    }
}