#if 0
package streets;

class PointStore {
    private var _listStreetPoints: List<StreetPoint>;

    public function findCloserThan( 
        sp0: StreetPoint,
        minDist: Float
    ): StreetPoint {
        var minDist2 = minDist*minDist;
        for( sp in _listStreetPoints ) {
            if( sp0 != sp ) {
                var dx = (sp0.pos.x-sp.pos.x);
                var dy = (sp0.pos.y-sp.pos.y);
                if (minDist2 > dx*dx+dy*dy) {
                    return sp;
                }
            }
        }
        return null;
    }


    public function removeByStreetPoint(sp: StreetPoint) {
        _listStreetPoints.remove(sp);
    }


    public function add( sp: StreetPoint ): Void {
        _listStreetPoints.add(sp);
    }


    public function new () {
        _listStreetPoints = new List<StreetPoint>();
    }

}
#end