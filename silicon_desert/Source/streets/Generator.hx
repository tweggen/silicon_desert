package streets;

import geom.Point;

class Generator {
    private var _listStrokesToDo: List<Stroke>;
    private var _strokeStore: StrokeStore;
    private var _traceGenerator = false;

    private var _generationCounter: Int;
    private var _rnd: RandomSource;

    private var _bl: Point;
    private var _tr: Point;


    /**
     * Return the primary direction at the given point.
     */
    public function primaryVector(origin: Point): Point {
        return new Point(0., 1.);
    }

    /**
     * Return the secondary direction at the given point.
     */
    public function secondaryVector(origin: Point): Point {
        return new Point(1., 0.);
    }


    /**
     * Find the first stroke in the list the candidate intersects with
     * and return it.
     */
    private function inBounds(cand: Stroke) : Bool {
        return !(false
            || cand.a.pos.x < _bl.x
            || cand.a.pos.y < _bl.y
            || cand.a.pos.x > _tr.x
            || cand.a.pos.y > _tr.x);
    }


    /**
     * Iterate until the queue of strokes is empty again.
     */
    public function generate(): Void {
        while(true) {

            if( 500<_generationCounter ) {
                if( _traceGenerator ) trace( 'Generator: Returning: max generations reached.' );
                return;
            }

            if( _listStrokesToDo.isEmpty() ) {
                if( _traceGenerator ) trace( 'Generator: Returning: no more streets to do.' );
                return;
            }

            var curr = _listStrokesToDo.pop();
            // trace( 'Generator: Starting new generation.' );

            /*
             * Check, wether this segment is valid.
             */

            /*
             * In bounds of the desired area?
             */
            if( !inBounds(curr) ) {
                if( _traceGenerator ) trace('Generator: Out of bounds: ${curr.toString()}');
                /*
                 * Out of range, so discard it.
                 */
                continue;
            }

            /*
             * Is there a street point close enough to use it?
             *
             * (in this implementation we assume no other pair of street points
             * is too close to each other).
             */

            /*
             * TXWTODO: This actually is the wrong name. The proper would be: Stop to
             * loop and do not add the current thing.
             */
            var continueCheck = true;
            var doAdd = true;

            while(continueCheck) {

                /*
                 * Whether we need to update the stroke metainfo because we modify
                 * its startpoint.
                 */
                var strokeNeedsUpdate = false;

                /*
                 * Check first: Is any of our endpoints too close to an existing endpoint?
                 */
                {
                    var tooClose: StreetPoint = _strokeStore.findCloserThan( 
                        curr.a, 20.0, curr.b );

                    if( null != tooClose ) {
                        if( _traceGenerator ) trace('Generator: StreetPoint (${curr.a.pos.x},${curr.a.pos.y}) too close '
                            + 'to StreetPoint (${tooClose.pos.x},${tooClose.pos.y}) ');
                        /*
                         * if a is close to another existing point, use that one.
                         */
                        curr.a = tooClose;
                        continue;
                    }
                }
                {
                    var tooClose: StreetPoint = _strokeStore.findCloserThan(
                        curr.b, 20.0, curr.a );

                    if( null != tooClose ) {
                        if( _traceGenerator ) trace('Generator: StreetPoint (${curr.a.pos.x},${curr.a.pos.y}) too close '
                            + 'to StreetPoint (${tooClose.pos.x},${tooClose.pos.y}) ');
                        /*
                         * if a is close to another existing point, use that one.
                         */
                        curr.b = tooClose;
                        continue;
                    }
                }
                if( curr.a.inStore && curr.b.inStore ) {
                    if( _strokeStore.areConnected(curr.a, curr.b)) {
                        doAdd = false;
                        continueCheck = false;
                        break;
                    }
                }
                {
                    var si = _strokeStore.getClosestPoint( curr );
                    if( si != null && si.scaleExists < 15.0 ) {
                        if( _traceGenerator ) trace( 'Generator: Discarding stroke, too close: ${si.scaleExists}' );
                        /*
                         * If there is any point closer the d meters to this stroke,
                         * then [look, which point is closer to the stroke and connect
                         * it instead] drop it.
                         */
                        doAdd = false;
                        continueCheck = false;
                        break;
                    }
                }

                /*
                 * Neither of the endpoints is too close to an existing one.
                 * However, this new stroke still could intersect with another 
                 * stroke. Test this.
                 */
                var intersection  = _strokeStore.intersectsMayTouch(curr);
                if( null != intersection ) {
                    /*
                     * We have an intersection of this stroke with another
                     * stroke.
                     *
                     * Given the way that we emit strokes we know, that curr.a
                     * is one StreetPoint of an existing stroke.
                     *
                     * In every case we will need to split the existing stroke into
                     * two, adding a new streetpoint at the intersection. If curr has a 
                     * higher weight than the existing one, curr also is added (in two parts).
                     * Otherwise, curr simply stops at the intersection point.
                     */

                    var intersectionStreetPoint = new StreetPoint();
                    intersectionStreetPoint.setPos( intersection.pos.x, intersection.pos.y );

#if 1
                    /*
                     * Well, still this intersection point might be closer to another
                     * known street point. If it does, we use the established point instead of the
                     * intersection point. Which, you guess, might yield to a different intersection...
                     */
                    var tooClose: StreetPoint = _strokeStore.findCloserThan( intersectionStreetPoint, 20.0, null );

                    if( tooClose != null ) {
                        /*
                         * The intersection is pretty close to another street point. Given,
                         * that the network was in a sane state before, it should remain sane enough if
                         * I replace this intersection with the existing street point for both
                         * the existing and the new stroke.
                         *
                         * TXWTODO: Add a new condition to check for street points near strokes
                         * while 1st path insertion.
                         */

                        // Do not add this stroke, but remove it.
                        doAdd = false;
                        continueCheck = false;
                        break;
                    }
#end

                    var oldStrokeExists = intersection.strokeExists;
                    var newStrokeExists = intersection.strokeExists.createUnattachedCopy();

                    /*
                     * Important: We must not modify the topology of the graph directly.
                     * Therefore we first remove the edge from the graph. Modifying the nodes
                     * and then readding it.
                     */
                    _strokeStore.remove( intersection.strokeExists );

                    oldStrokeExists.b = intersectionStreetPoint;
                    newStrokeExists.a = intersectionStreetPoint;

                    _strokeStore.add(newStrokeExists);
                    _strokeStore.add(oldStrokeExists);

                    curr.b = intersectionStreetPoint;
                    _strokeStore.add(curr);

                    // Leave this loop.
                    doAdd = false;
                    continueCheck = false;

                    // TXWTODO: Add the continuation of curr.

                    // Bail out at this point.
                    // break;

                    // Yeah, finally we should recheck.
                    // continue;
                }

                /*
                 * If we reached this point, we are clean. No streetpoint closer to another
                 * streetpoint, plus this stroke is not intersecting another one.
                 */
                break;

            }

            if( !doAdd ) {
                // trace( 'Generator: Avoiding to add stroke.' );
                continue;
            }



            /*
             * Add the stroke to the map, creating a continuation and
             * pronably side streets.
             */
            _strokeStore.add(curr);
            ++_generationCounter;

            /*
             * Compute some options.
             */
            var doForward: Bool = true || _rnd.getFloat() >= 0.01;
            var doRight: Bool = _rnd.getFloat() > (curr.weight*0.6);
            var doLeft: Bool = _rnd.getFloat() > (curr.weight*0.6);
            var doRandomDirection: Bool = _rnd.getFloat() < (curr.weight*0.1);
            var doIncreaseWeight: Bool = _rnd.getFloat() < 0.03;
            var doDecreaseWeight: Bool = _rnd.getFloat() < 0.30;

            var newWeight = curr.weight;
            var newLength = 40 + 40 * (newWeight*newWeight);

            {
                if( doIncreaseWeight ) {
                    newWeight *= 1.10;
                }
                if( doDecreaseWeight ) {
                    newWeight *= 0.90;
                }
                if( newWeight<0.1 ) {
                    newWeight = 0.1;
                } else {
                    if( newWeight>1.9 ) {
                        newWeight = 1.9;
                    }
                }
            }

            var newAngle = curr.angle;
            if (_rnd.getFloat()>0.4 ) {
                newAngle = newAngle +_rnd.getFloat()*0.02-0.01;
            }

            if(!doForward && !doRight && !doLeft && !doRandomDirection) {
                doRandomDirection = true;
            }

            if( doForward ) {
                var newB = new StreetPoint();
                var forward = Stroke.createByAngleFrom(
                    curr.b,
                    newB,
                    newAngle,
                    newLength,
                    curr.isPrimary,
                    newWeight
                );
                _listStrokesToDo.add(forward);
            }
            if( doRight ) {
                var newB = new StreetPoint();
                var right = Stroke.createByAngleFrom(
                    curr.b,
                    newB,
                    newAngle-Math.PI/2.0,
                    newLength,
                    !curr.isPrimary,
                    newWeight
                );
                _listStrokesToDo.add(right);
            }
            if( doLeft ) {
                var newB = new StreetPoint();
                var left = Stroke.createByAngleFrom(
                    curr.b,
                    newB,
                    newAngle+Math.PI/2.0,
                    newLength,
                    !curr.isPrimary,
                    newWeight
                );
                _listStrokesToDo.add(left);
            }
            if( doRandomDirection ) {
                var newB = new StreetPoint();
                var randStroke = Stroke.createByAngleFrom(
                    curr.b,
                    newB,
                    _rnd.getFloat()*Math.PI*2.0,
                    newLength,
                    curr.isPrimary,
                    newWeight
                );
                _listStrokesToDo.add(randStroke);
            }
        }
    }


    public function setBounds( 
        blx0: Float,
        bly0: Float,
        trx0: Float,
        try0: Float
    ) {
        _tr = new Point( trx0, try0 );
        _bl = new Point( blx0, bly0 );
    }


    public function getStrokes() {
        return _strokeStore.getStrokes();
    }


    public function addStartingStroke(stroke0: Stroke): Void {
        _listStrokesToDo.add(stroke0);
    }

    public function reset(
        seed0: String,
        strokeStore: StrokeStore
    ): Void {
        _rnd = new RandomSource(seed0);
        _listStrokesToDo = new List<Stroke>();
        _strokeStore = strokeStore;
        _generationCounter = 0;
    }


    public function new () {
    }
}
