package streets;


import geom.Point;


class StreetPoint {
    public var pos(default, null): Point;

    /*
     * Setup insync with the StrokeStore._listPoints
     */
    public var inStore: Bool;

    private var _listStartingStrokes: List<Stroke>;
    private var _listEndingStrokes: List<Stroke>;

    private var _angleArray: Array<Stroke>;

    public function setPos(x: Float, y: Float) {
        pos.x = x;
        pos.y = y;
    }


    static private function snorm(a: Float): Float {
        while(a<-Math.PI) {
            a += Math.PI * 2.0;
        }
        while(a>Math.PI) {
            a -= Math.PI * 2.0;
        }
        return a;
    }
    static private function unorm(a: Float): Float {
        while(a<0.0) {
            a += Math.PI * 2.0;
        }
        while(a>2*Math.PI) {
            a -= Math.PI * 2.0;
        }
        return a;
    }


    public function getAngleArray(): Array<Stroke> {
        if(null != _angleArray) {
            return _angleArray;
        }
        _angleArray = new Array<Stroke>();
        if(null != _listStartingStrokes) {
            for(stroke in _listStartingStrokes) {
                if(null==stroke) {
                    throw('StreetPoint:getAngleArray(): Refusing to add null stroke.');
                }
                _angleArray.push(stroke);
            }
        }
        if(null != _listEndingStrokes) {
            for(stroke in _listEndingStrokes) {
                if(null==stroke) {
                    throw('StreetPoint:getAngleArray(): Refusing to add null stroke.');
                }
                _angleArray.push(stroke);
            }
        }
        _angleArray.sort(function(a: Stroke, b: Stroke): Int {
            var diff = snorm( a.angle - b.angle );
            if( diff<0.0 ) return -1;
            else if(diff>0.0) return 1;
            else return 0;
        });
        return _angleArray;
    }   


    /**
     * Given the angle from another incoming stroke, find the next outgoing
     * stroke.
     */
    public function getNextAngle(strokeCurrent: Stroke, angle: Float, clockwise: Bool): Stroke {
        var minAngle: Float = Math.PI * 2.0;
        var minStroke: Stroke = null;
        var nullStroke: Stroke = null;
        if( null==strokeCurrent ) {
            throw 'StreetPoint.getNextAngle(): Called without current stroke.';
        }

        var debugPoint = false;
        if( true ) {
            var xDif = pos.x - (56.3645166477334);
            if( xDif < 0.01 && xDif > -0.01 ) {
                debugPoint = true;
            }
        }

        /*
         * Also, per API, we take the angle of an incoming stroke.
         * However, we want to find the next outgoing stroke, so we need 
         * to inverse the angle, as we will compute outgoing angles
         * in this function.
         */
        var myAngle = snorm(angle + Math.PI);

        if( !clockwise ) {
            throw 'StreetPoint:getNextAngle(): Anti-Clockwise not implemented yet.';
        }

        /*
         * Start with the outgoing strokes.
         */
        if( null != _listStartingStrokes ) {
            for( stroke in _listStartingStrokes ) {
                var currAngle = snorm(stroke.angle);
                /*
                 * Note, that we need to use the unsigned angle.
                 */
                var diffAngle = unorm(currAngle - myAngle);

                var isStart: Bool = (strokeCurrent != null) && (strokeCurrent == stroke);

                if( debugPoint ) {
                    trace('getNextAngle($pos, $myAngle, ${stroke.b.pos.x}): OUT ${isStart?"Start ":""}$currAngle diffAngle $diffAngle');
                }

                if( isStart ) {
                    /* 
                     * This must be the same storke.
                     */
                    nullStroke = stroke;
                } else if( minAngle > diffAngle ) {
                    /*
                     * A new smaller one.
                     */
                    minStroke = stroke;
                    minAngle = diffAngle;
                }

            }
        }


        /*
         * Now the incoming strokes. Their angles need to be inversed.
         */
        if( null != _listEndingStrokes ) {
            for( stroke in _listEndingStrokes ) {
                /*
                 * Note the offset.
                 */
                var currAngle = snorm(stroke.angle + Math.PI);
                /*
                 * Note, that we need to use the unsigned angle for minimizing
                 * angle.
                 */
                var diffAngle = unorm(currAngle - myAngle); 

                var isStart: Bool = (strokeCurrent != null) && (strokeCurrent == stroke);

                if( debugPoint ) {
                    trace('getNextAngle($pos, $myAngle, ${stroke.a.pos.x}): IN ${isStart?"Start ":""} $currAngle diffAngle $diffAngle');
                }

                if( isStart ) {
                    /* 
                     * This must be the same storke.
                     */
                    nullStroke = stroke;
                } else if( minAngle > diffAngle ) {
                    /*
                     * A new smaller one.
                     */
                    minStroke = stroke;
                    minAngle = diffAngle;
                }

            }
        }

        // Return null or myself.
        return minStroke;
    }


    public function removeStartingStroke(s: Stroke) {
        if( null==_listStartingStrokes ) {
            throw 'StreetPoint: No Starting list yet.';
        }
        _angleArray = null;
        _listStartingStrokes.remove(s);
    }


    public function addStartingStroke(s: Stroke) {
        if( null==_listStartingStrokes ) {
            _listStartingStrokes = new List<Stroke>();
        }
        _angleArray = null;
        if( !_listStartingStrokes.filter(function(a) { return a==s; }).isEmpty() ) {
            throw 'StreetPoint.addStartingStroke(): Stroke ${s.toString()} already attached.';
        }
        _listStartingStrokes.add( s );
    }


    public function removeEndingStroke(s: Stroke) {
        if( null==_listEndingStrokes ) {
            throw 'StreetPoint: No Ending list yet.';
        }
        _angleArray = null;
        _listEndingStrokes.remove(s);
    }


    public function addEndingStroke(s: Stroke) {
        if( null==_listEndingStrokes ) {
            _listEndingStrokes = new List<Stroke>();
        }
        _angleArray = null;
        if( !_listEndingStrokes.filter(function(a) { return a==s; }).isEmpty() ) {
            throw 'StreetPoint.addStartingStroke(): Stroke ${s.toString()} already attached.';
        }
        _listEndingStrokes.add( s );
    }


    public function hasStrokes(): Bool {
        if( 
            (
                null==_listStartingStrokes || _listStartingStrokes.isEmpty()
            ) && (
                null==_listEndingStrokes || _listEndingStrokes.isEmpty()
            )
        ) {
            return false;
        } else {
            return true;
        }
    }


    public function new () {
        pos = new Point( 0., 0. );
        inStore = false;
        _listStartingStrokes = null;
        _listEndingStrokes = null;
        _angleArray = null;
    }
}
