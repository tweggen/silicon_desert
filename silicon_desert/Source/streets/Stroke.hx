package streets;

import geom.Point;

/**
 * Represent one stroke of street. Streets are generated stroke by stroke.
 */
class Stroke {
    static private var nextId: Int = 0;

    public var sid: Int;

    public var store: StrokeStore;

    /**
     * The weight of this stroke: Import streets will have a higher weight, 
     * some side alleys will have lower weight.
     */
    public var weight: Float;


    /**
     * Wether this one goes in primary or secondary direction.
     */
    public var isPrimary: Bool;


    /**
     * The angle of the street.
     */
    public var angle: Float;


    /**
     * The length of the stroke.
     */
    public var length: Float;


    /**
     * The point this stroke is coming from.
     */
    public var a(default, set): StreetPoint;


    /**
     * The point this stroke is going to.
     */
    public var b(default, set): StreetPoint;


    /**
     * Several graph algorithms will try to traveres throught the 
     * strokes. They need to remember which ones they already 
     * encountered.
     */
    public var traversedAB: Bool;

    /**
     * Several graph algorithms will try to traveres throught the 
     * strokes. They need to remember which ones they already 
     * encountered.
     */
    public var traversedBA: Bool;


    private function _updateGeom(): Void {
        var dx = b.pos.x - a.pos.x;
        var dy = b.pos.y - a.pos.y;

        length = Math.sqrt(dx*dx+dy*dy);
        if(length<0.000001) {
            throw 'Stroke._updateGeom(): Zero length stroke.';
        }

        /*
         * Make it a unit vector to have better NaN thesholds
         */
        dx = dx / length;
        dy = dy / length;
        var phi: Float;

        if( dx>0.000001 ) {
            if( dy>=0. ) {
                phi = Math.atan(dy/dx);
            } else {
                phi = Math.atan(dy/dx);
            }
        } else if( dx<-0.000001 ) {
            if( dy>=0. ) {
                phi = Math.atan(dy/dx) + Math.PI;
            } else {
                phi = Math.atan(dy/dx) - Math.PI;
            }
        } else {
            if( dy>=0. ) {
                phi = Math.PI/2.0;
            } else {
                phi = -Math.PI/2.0;
            }
        }
        angle = phi;
    }


    public function set_a(sp: StreetPoint): StreetPoint {
        if( null != store ) {
            throw 'Stroke: Tried to exchange endpoint while in graph.';
        }
        a = sp;
        if( a != null && b != null ) {
            _updateGeom();
        }
        return sp;
    }


    public function set_b(sp: StreetPoint): StreetPoint {
        if( null != store ) {
            throw 'Stroke: Tried to exchange endpoint while in graph.';
        }
        b = sp;
        if( a != null && b != null ) {
            _updateGeom();
        }
        return sp;
    }


    /**
     * Stolen from https://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect
     */
    public function intersects(o: Stroke): StrokeIntersection {
        if( null==o ) {
            throw 'Stroke: intersect arg is null.';
        }
        var p0_x = a.pos.x;
        var p0_y = a.pos.y;
        var p1_x = b.pos.x;
        var p1_y = b.pos.y;

        var p2_x = o.a.pos.x;
        var p2_y = o.a.pos.y;
        var p3_x = o.b.pos.x;
        var p3_y = o.b.pos.y;

        var i_x = 0.;
        var i_y = 0.;
        var does = false;

        var s1_x, s1_y, s2_x, s2_y: Float;

        s1_x = p1_x - p0_x;     s1_y = p1_y - p0_y;
        s2_x = p3_x - p2_x;     s2_y = p3_y - p2_y;

        var s, t: Float;
        s = (-s1_y * (p0_x - p2_x) + s1_x * (p0_y - p2_y)) / (-s2_x * s1_y + s1_x * s2_y);
        t = ( s2_x * (p0_y - p2_y) - s2_y * (p0_x - p2_x)) / (-s2_x * s1_y + s1_x * s2_y);

        // if (s >= 0 && s <= 1 && t >= 0 && t <= 1) {
        if (s >= 0 && s <= 1 && t >= 0 && t <= 1) {
            // Collision detected
            i_x = p0_x + (t * s1_x);
            i_y = p0_y + (t * s1_y);

            var intersection = new StrokeIntersection();
            intersection.strokeCand = o;
            intersection.scaleCand = s;
            intersection.strokeExists = this;
            intersection.scaleExists = t;
            intersection.pos = new Point( i_x, i_y );

            return intersection;
        }

        return null; // No collision
    }


    /**
     * Compute the distance of the given point to this stroke.
     */
    public function distance(px: Float, py:Float) : Float {
        /*
         * distance := abs((p-a) X b) / abs(b)
         */
        var pax = px - a.pos.x;
        var pay = py - a.pos.y;

        var cross = pax * b.pos.y - pay * b.pos.x;
        var absb = Math.sqrt(b.pos.x*b.pos.x+b.pos.y*b.pos.y);
        var dist = cross / absb;
        return dist;
    }

    private function new () {
        sid = ++nextId;
        a = null; //new StreetPoint();
        b = null; //new StreetPoint();
        store = null;
        traversedAB = false;
        traversedBA = false;
    }


    private function _copyMetaFrom(o: Stroke) {
        isPrimary = o.isPrimary;
        weight = o.weight;
    }


    /**
     * Create a copy of this object.
     *
     * The points are used as in this stroke.
     * Howevere, the stroke is not added to the stroke store and therefore
     * also not added to the StreetPoints.
     */
    public function createUnattachedCopy(): Stroke
    {
        var stroke = new Stroke();
        stroke.a = a;
        stroke.b = b;
        stroke.store = null;
        stroke._copyMetaFrom(this);
        _updateGeom();
        return stroke;
    }


    /**
     * Create a new stroke from the given street point.
     * Compute the coordinates for the target streetpoint.
     */
    public static function createByAngleFrom(
        a0: StreetPoint,
        b0: StreetPoint,
        angle0: Float,
        length0: Float,
        isPrimary0: Bool,
        weight0: Float
    ): Stroke {
        if(angle0 > Math.PI) {
            angle0 -= 2*Math.PI;
        } else if( angle0<-Math.PI) {
            angle0 += 2*Math.PI;
        }
        var stroke = new Stroke();

        b0.setPos(
            a0.pos.x + Math.cos(angle0)*length0,
            a0.pos.y + Math.sin(angle0)*length0
        );
        stroke.a = a0;
        stroke.b = b0;

        stroke.angle = angle0;
        stroke.length = length0;
        stroke.isPrimary = isPrimary0;
        stroke.weight = weight0;

        return stroke;
    }


    public function toString(): String {
        var ax = a.pos.x;
        var ay = a.pos.y;
        var bx = b.pos.x;
        var by = b.pos.y;
        return '$sid: ($ax, $ay)-($bx, $by)';
    }
}
