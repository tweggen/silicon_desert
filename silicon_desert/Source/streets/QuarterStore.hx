package streets;

class QuarterStore {
    private var _listQuarters: List<Quarter>;

    public function add(quarter: Quarter) {
        _listQuarters.add( quarter );
    }

    public function getQuarters(): List<Quarter> {
        return _listQuarters;
    }

    public function new () {
        _listQuarters = new List<Quarter>();
    }
}