package streets;

import geom.Point;

/**
 * Represent the result of a stroke intersection.
 */
class StrokeIntersection {
    public var pos: Point;
    public var streetPoint: StreetPoint;

    public var strokeCand: Stroke;
    public var scaleCand: Float;

    public var strokeExists: Stroke;
    public var scaleExists: Float;

    public function new () {
        pos = null;
        streetPoint = null;
        strokeCand = null;
        scaleCand = 0.0;
        strokeExists = null;
        scaleExists = 0.0;
    }
}