package streets;

import geom.Point;


/**
 * A street quarter represents the abstract area between streets.
 * It is defined by street points in a clock-wise direction.
 */
class Quarter {

    private var _delims: List<QuarterDelim>;

    public function addQuarterDelim( quarterDelim: QuarterDelim ) {
        _delims.add( quarterDelim );
    }

    public function getDelims(): List<QuarterDelim> {
        return _delims;
    }

    public function new () {
        _delims = new List<QuarterDelim>();
    }
}
