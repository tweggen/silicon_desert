package streets.graph;

using streets.graph.Node;

class Edge {
    public id(default,null): String;

    public var nodeFrom(default, null): Node;
    public var nodeTo(default, null): Node;

    public operator new (
        id0: String,
        nodeFrom0: Node,
        nodeTo0: Node
    )
    {
        id = id0;
        nodeFrom = nodeFrom0;
        nodeTo = nodeTo0;
    }
}