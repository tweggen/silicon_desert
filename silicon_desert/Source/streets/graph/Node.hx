package streets.graph;

using streets.graph.Edge;
using geom.Point;

/**
 * Represent one junction in the street network.
 * One edge between two nodes represents one street with exactly the same
 * properties.
 */
class Node {
    public var id(default, set): String;

    private var _mapEdges: Map<String, Edge>;

    public var position(default,null): Point;
    private var set_position(position0: Point): Void {
        // TXWTODO: Invalidate all geometry maps in graph.
        this.position = position0;
    }


    /**
     * If we have an edge to the given node, return the
     * edge.
     */
    public function getEdgeTo(id: String) : Edge {
        if (null == id || 0==id.length) {
            throw  'Graph.getEdgeTo(): id is null.';
        }
        var edge = _mapEdges.get(id);
        if (null==edge) {
            return null;
        } else {
            return edge;
        }
    }


    public function new (id0: String)
    {
        id = id0;
        _mapEdges = new Map<String, Edge>();
    }
}
