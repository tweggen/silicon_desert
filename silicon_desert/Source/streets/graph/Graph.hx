package streets.graph;

using streets.graph.Node;
using streets.graph.Edge;

using geom.Point;

/**
 * Represent a street map. This contains a street network graph.
 * The graph may be disjoint.
 *
 * The graph hierarchy of data structures (Graph, Node, Edge) contains the
 * plain graph information. Any additional geometrical information, e.g. for
 * computing intersections, is kept in separate data structures.
 */
class Graph {

    private var _mapNodes: Map<String,Node>;

    private var _nextPointId: Int;
    private function _createPointId(): String {
        ++_nextPointId;
        var id: String = 'sgn$_nextPointId';
        return id;
    }

    public function createNode(options: {
        id: String,
        position: Point
    }): Node {
        if (null==id || id=="") {
            id = createPointId();
        }
        var node = new Node(id);
        if(null!=position) {
            node.position = position;
        }
        _mapNodes.set(id, node);
        return node;
    }

    public function createEdge(options: {
        id: String,
        idNodeFrom: String,
        idNodeTo: String,
        nodeFrom: Node,
        nodeTo: Node
    }) : Edge {
        
    }


    public function getNode(id: String) : Node {
        if (null == id || 0==id.length) {
            throw  'Graph.getNode(): id is null.';
        }
        var edge = _mapNodes.get(id);
        if (null==edge) {
            return null;
        } else {
            return edge;
        }
    }

    public function new ()
    {
        _mapNodes = new Map<String, Edge>();
        _nextPointId = 10000;
    }
}