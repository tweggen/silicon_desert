package;


import away3d.cameras.*;
import away3d.containers.*;
import away3d.controllers.*;
import away3d.debug.*;
import away3d.entities.Mesh;
import away3d.extrusions.*;
import away3d.filters.BloomFilter3D;
import away3d.filters.DepthOfFieldFilter3D;
import away3d.lights.*;
import away3d.materials.*;
import away3d.materials.lightpickers.*;
import away3d.materials.methods.*;
import away3d.primitives.*;
import away3d.textures.*;
import away3d.utils.*;

import away3d.core.base.Geometry;
import away3d.core.base.SubGeometry;

import openfl.display.*;
import openfl.events.*;
import openfl.filters.*;
import openfl.text.*;
import openfl.ui.*;

import openfl.Vector;

/**
 * Prototype for a generic object put somewhere.
 * Currently, this always is a house.
 */
class PlaceObject
{
    private var _rnd:RandomSource;
    private var _myKey:String;
    
    private var _matBuilding:TextureMaterial;
    private var _o3dHouse: ObjectContainer3D;
    // private var _o3dMesh;

    private var _myParent:ObjectContainer3D;

    private var _allEnv:AllEnv;

    private var _frag:WorldFragment;

    public var width(default,null):Float;
    public var height(default,null):Float;
    public var depth(default,null):Float;

    private function p(
        a: Vector<Float>, 
        x: Float, y: Float, z: Float
    ): Void {
        a.push(x);
        a.push(y);
        a.push(z);
    }

    private function uv(
        a: Vector<Float>, 
        u: Float, v: Float
    ): Void {
        a.push(u);
        a.push(v);
    }

    private function idx(
        a: Vector<UInt>, 
        p1: UInt, p2: UInt, p3: UInt
    ): Void {
        a.push(p1);
        a.push(p2);
        a.push(p3);
    }

    public function thingUnload()
    {
        _o3dHouse = null;
        _matBuilding = null;
    }

    public function thingSetup( envCurrent:AllEnv)
    {
        _rnd.clear();

        _allEnv = envCurrent;
        height = 5.0 + _rnd.getFloat() * 80.;
        width = 5.0 + _rnd.getFloat() * 15.;
        depth = 5.0 + _rnd.getFloat() * 15.;
    }

    public function thingShutdown()
    {
        _rnd = null;
        _allEnv = null;
    }

    public function thingLoad()
    {
        _matBuilding = WorldMetaGen.cat.catGetSingleton( 
            "PlaceObject._matBuilding", function () {
                return new materials.HouseMaterial(_allEnv);
            }).texGet();

        _o3dHouse = new ObjectContainer3D();

        var meshHouseMain: Mesh;
        /*
         * A house segment in this configuration can be at most
         * 20 by 80 per side (that's our definition).
         */
        {
            var geometry:Geometry = new Geometry();

            var subGeometry:SubGeometry = new SubGeometry();
            subGeometry.autoDeriveVertexNormals = true;
            subGeometry.autoDeriveVertexTangents = true;
            geometry.addSubGeometry( subGeometry );

            var maxWidth = 20.;
            var maxHeight = 80.;
            var maxDepth = 20.;

            var w = width;
            var h = height;
            var d = depth;
            if( w>maxWidth ) w = maxWidth;
            if( h>maxHeight ) h = maxHeight;
            if( d>maxDepth ) d = maxDepth;

            var v = new Vector<Float>();
            var i = new Vector<UInt>();
            var u = new Vector<Float>();

            p(v, 0., 0., 0.);
            uv(u, 0./80., 0./80.);
            p(v, 0., h, 0.);
            uv(u, 0./80., h/80.);
            p(v, w, h, 0.);
            uv(u, w/80., h/80.);
            p(v, w, 0., 0.);
            uv(u, w/80., 0./80.);
            p(v, 0., 0., d);
            uv(u, d/80., 0./80.);
            p(v, 0., h, d);
            uv(u, d/80., h/80.);
            p(v, w, h, d);
            uv(u, (d+w)/80., h/80.);
            p(v, w, 0., d);
            uv(u, (d+w)/80., 0./80.);
            // And double the right column for proper tecturing
            p(v, w, h, 0.);
            uv(u, (d+w+d)/80., h/80.);
            p(v, w, 0., 0.);
            uv(u, (d+w+d)/80., 0./80.);


            // Front
            idx(i, 1, 2, 0);
            idx(i, 2, 3, 0);
            // Back
            idx(i, 6, 5, 4);
            idx(i, 7, 6, 4);
            // Left
            idx(i, 5, 1, 0);
            idx(i, 4, 5, 0);
            // Right
            idx(i, 6, 7, 9);
            idx(i, 8, 6, 9);

            subGeometry.updateVertexData( v );
            subGeometry.updateIndexData( i );
            subGeometry.updateUVData( u );

            meshHouseMain = new Mesh( geometry, _matBuilding );
            meshHouseMain.x = -width/2.0;
            meshHouseMain.y = 0.;
            meshHouseMain.z = -depth/2.0;
        }
        _o3dHouse.addChild( meshHouseMain );

        var meshBorder = new Mesh(
            new PlaneGeometry(0.5, height, 1, 1, false, true),
            new away3d.materials.ColorMaterial(0x66cccc));
        meshBorder.x = width/2.0 - 0.5;
        meshBorder.y = height/2.0;
        meshBorder.z = depth/2.0+0.01;
        _o3dHouse.addChild( meshBorder );        
    }


    /**
     * Set the main position of the object.
     */
    public function thingPlace(
            xo:Float, yo:Float, zo:Float,
            x:Float, y:Float, z:Float )
    {
        _o3dHouse.x = x;
        _o3dHouse.z = z;
        /*
         * Compute the base height by probing at all four corners.
         */
        var baseHeight: Float = 10000;
        var f;
        try {
            f = _allEnv.worldLoader.worldLoaderGetHeightAt( 
                xo+_o3dHouse.x-width/2.0, zo+_o3dHouse.z-depth/2.0 );
            if (f<baseHeight) baseHeight = f;
        } catch(exc: Dynamic) {}
        try {
            f = _allEnv.worldLoader.worldLoaderGetHeightAt( 
                xo+_o3dHouse.x+width/2.0, zo+_o3dHouse.z-depth/2.0 );
            if (f<baseHeight) baseHeight = f;
        } catch(exc: Dynamic) {}
        try {
            f = _allEnv.worldLoader.worldLoaderGetHeightAt( 
                xo+_o3dHouse.x-width/2.0, zo+_o3dHouse.z+depth/2.0 );
            if (f<baseHeight) baseHeight = f;
        } catch(exc: Dynamic) {}
        try {
            f = _allEnv.worldLoader.worldLoaderGetHeightAt( 
                xo+_o3dHouse.x+width/2.0, zo+_o3dHouse.z+depth/2.0 );
            if (f<baseHeight) baseHeight = f;
        } catch(exc: Dynamic) {}
        _o3dHouse.y = baseHeight + yo+y;
    }


    public function thingRemove( parent0:ObjectContainer3D )
    {
        _myParent = null;
    }


    public function thingAdd( parent0:ObjectContainer3D )
    {
        if(null==parent0) {
            throw 'PlaceObject.thingAdd(parent): parent is null.';
        }
        _myParent = parent0;
        _myParent.addChild( _o3dHouse );
    }


    public function new ( 
            newKey:String, 
            frag:WorldFragment )
    {
        _frag = frag;
        _myKey = newKey;
        _rnd = new RandomSource( _myKey );
    }
}
