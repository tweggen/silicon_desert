package loaders;

class Load3ds
{
    static public function load( urlLocator0:String, idObj3d:String, onComplete: Void->Void ): loaders.Load3dsContext
    {
        var loader = new Load3dsContext( urlLocator0, idObj3d, onComplete );
        loader.start();
        return loader;
    }
}