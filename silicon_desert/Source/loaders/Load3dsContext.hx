package loaders;

import openfl.Assets;

class Load3dsContext 
{
    private var _urlLocator: String = "";
    private var _idObj3d: String = "";
    private var state: String = "";
    // private obj3d: away3d.containers.ObjectContainer3D = null;

    /**
     * Called as soon the world meta generator has completed.
     * Callback is provided to the constructor.
     */
    private var _onComplete:Void->Void;

    /**
     * This is the actual loader object
     */
    private var _loader3d:away3d.loaders.Loader3D;

    private var _traceLoadAsset = true;


    private function on3DAssetComplete(
            event:away3d.events.Asset3DEvent ) : Void
    {
        if( _traceLoadAsset ) trace( event );
        if( _traceLoadAsset ) trace( "Loaded " + event.asset.name + " Name: " + event.asset.name );
    }
    

    private function on3DContainerComplete(
            event:away3d.events.Asset3DEvent ) : Void
    {
        if( _traceLoadAsset ) trace( event );
        if( _traceLoadAsset ) trace( "Container " + event.asset.name + " Name: " + event.asset.name );
    }
    

    private function on3DMeshComplete(
            event:away3d.events.Asset3DEvent ) : Void
    {
        if( _traceLoadAsset ) trace( event );
        if( _traceLoadAsset ) trace( "Loaded " + event.asset.name + " Name: " + event.asset.name );
    }
    

    private function on3DLoadError(
            event:away3d.events.LoaderEvent ) : Void
    {
        if( _traceLoadAsset ) trace( event );
        if( _traceLoadAsset ) trace( "Error " + event.message );
        remove3dCallbacks( _loader3d );

        if( _onComplete != null ) {
            _onComplete();
        }
    }
    

    private function on3DResourceComplete(
            event:away3d.events.LoaderEvent ) : Void
    {
        if( _traceLoadAsset ) trace( event );
        if( _traceLoadAsset ) trace( "Complete" );
        var container3d:away3d.containers.ObjectContainer3D = _loader3d;
        if( _traceLoadAsset ) trace( "Asset contains "+container3d.numChildren+" children." );
        if( _traceLoadAsset ) trace( "Asset type "+container3d.assetType );
        if( _traceLoadAsset ) trace( "Asset visible== "+container3d.visible+"." );
        if( _traceLoadAsset ) trace( "Asset extent X: "+container3d.minX+"..."+container3d.maxX );
        remove3dCallbacks( _loader3d );

        var o: away3d.containers.ObjectContainer3D = cast( _loader3d.clone(), away3d.containers.ObjectContainer3D );
        for(idx in 0...container3d.numChildren) {
            var child = o.getChildAt(idx);
            var id: String = (child.id!=null)?child.id:"null";
            var name: String = (child.name!=null)?child.name:"null";
            var originalName: String = (child.originalName!=null)?child.originalName:"null";
            trace( 'Object has id: $id, name: $name, originalName: $originalName');
        }
        WorldMetaGen.cat.catAddGlobalEntity( _idObj3d, o );

        if( _onComplete != null ) {
            _onComplete();
        }
    }
    

    private function remove3dCallbacks( loader3d:away3d.loaders.Loader3D )
    {
        loader3d.removeEventListener(
            away3d.events.Asset3DEvent.ASSET_COMPLETE, 
            on3DAssetComplete );
        loader3d.removeEventListener(
            away3d.events.Asset3DEvent.CONTAINER_COMPLETE, 
            on3DContainerComplete );
        loader3d.removeEventListener(
            away3d.events.Asset3DEvent.MESH_COMPLETE, 
            on3DMeshComplete );
        loader3d.removeEventListener(
            away3d.events.LoaderEvent.LOAD_ERROR, 
            on3DLoadError );
        loader3d.removeEventListener(
            away3d.events.LoaderEvent.RESOURCE_COMPLETE, 
            on3DResourceComplete );
    }


    private function add3dCallbacks( loader3d:away3d.loaders.Loader3D )
    {
        loader3d.addEventListener(
            away3d.events.Asset3DEvent.ASSET_COMPLETE, 
            on3DAssetComplete );
        loader3d.addEventListener(
            away3d.events.Asset3DEvent.CONTAINER_COMPLETE, 
            on3DContainerComplete );
        loader3d.addEventListener(
            away3d.events.Asset3DEvent.MESH_COMPLETE, 
            on3DMeshComplete );
        loader3d.addEventListener(
            away3d.events.LoaderEvent.LOAD_ERROR, 
            on3DLoadError );
        loader3d.addEventListener(
            away3d.events.LoaderEvent.RESOURCE_COMPLETE, 
            on3DResourceComplete );
    }


    public function start()
    {
        var assetLoaderContext:away3d.loaders.misc.AssetLoaderContext =
            new away3d.loaders.misc.AssetLoaderContext();
        try {
            add3dCallbacks( _loader3d );
            _loader3d.loadData(
                Assets.getBytes( _urlLocator ),
                assetLoaderContext );
        } catch( unknown:Dynamic ) {
            trace( "Unknown exception: "+Std.string( unknown )+ "\n"
                + haxe.CallStack.toString( haxe.CallStack.callStack() )
                + haxe.CallStack.toString( haxe.CallStack.exceptionStack() ) );
            remove3dCallbacks( _loader3d );
        }
    }


    public function new( urlLocator: String, idObj3d: String, onComplete: Void->Void )
    {
        _urlLocator = urlLocator;
        _idObj3d = idObj3d;
        _onComplete = onComplete;
        _loader3d = new away3d.loaders.Loader3D();
    }

}