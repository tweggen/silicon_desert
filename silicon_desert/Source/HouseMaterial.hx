
package;

import away3d.materials.*;

class HouseMaterial
    extends Entity

{
    private var _terrainEnv: AllEnv;
    private var _matHouse: TextureMaterial;

    public function texGet(): TextureMaterial {
        return _matHouse;
    }

    public function new(terrainEnv: AllEnv)
    {
        _terrainEnv = terrainEnv;
        //_matHouse = new TextureMaterial(away3d.utils.Cast.bitmapTexture("building/stealtiles1.jpg"));
        _matHouse = new TextureMaterial(
            away3d.utils.Cast.bitmapTexture("building/cyanwindow.jpg")
            // away3d.utils.Cast.bitmapTexture("building/yellowwindows.png")
        );
        _matHouse.lightPicker = _terrainEnv.allEnvironmentGetLightPicker();
        _matHouse.ambientColor = 0xffffff;
        _matHouse.ambient = 0.5;
        _matHouse.specular = 0;
        _matHouse.repeat = true;
        _matHouse.smooth = false;
        _matHouse.addMethod( _terrainEnv.allEnvironmentGetFogMethod() );
    }
}