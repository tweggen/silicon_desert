package geom;

class Point {
    public var x: Float;
    public var y: Float;

    public function new (x0: Float, y0: Float) {
        x = x0;
        y = y0;
    }
}
