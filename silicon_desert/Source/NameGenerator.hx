package;

/**
 * Yet absolutely unstructured class to generate names.
 * Nearly everything within is todo.
 */
class NameGenerator extends Entity
{
    private var _worldMetaGen: WorldMetaGen;

    private var _pSylOpen: Float = 0.75;
    private var _arrSylOpen: Array<String> = [
        "b", "d", "f", "g", "j", "k", "l", "m", "n", "p", "r", "s", "sh", "t", "v", "y"
    ];
    private var _pSylInMod: Float = 0.2;
    private var _arrSylInMod: Array<String> = [
        "l", "r", "s", "v", "y"
    ];
    private var _pSylVow: Float = 0.95;
    private var _arrSylVow: Array<String> = [
        "a", "e", "i", "o", "u", "ee"
    ];
    private var _pSylClose: Float = 0.4;
    private var _arrSylClose: Array<String> = [
        "ng", "kh", "f", "k", "l", "m", "p", "r", "s", "t"
    ];

    private function createPhoneme(rnd: RandomSource, prob: Float, arr: Array<String>): String {
        var r = rnd.getFloat();
        if(r<prob) {
            var idx: Int = Std.int(r*arr.length*(0.999/prob));
            return arr[idx];
        } else {
            return "";
        }
    }

    private function createSyllable(rnd: RandomSource): String {
        var strSyl: String = "";
        strSyl += createPhoneme(rnd, _pSylOpen, _arrSylOpen);
        strSyl += createPhoneme(rnd, _pSylInMod, _arrSylInMod);
        strSyl += createPhoneme(rnd, _pSylVow, _arrSylVow);
        strSyl += createPhoneme(rnd, _pSylClose, _arrSylClose);
        return strSyl;
    }

    private function createRawWord(rnd: RandomSource): String {
        var r = rnd.getFloat();
        var n:Int;
        if(r<0.05) {
            n = 1;
        } else if(r<0.4) {
            n = 2;
        } else if(r<0.7) {
            n = 3;
        } else if(r<0.85) {
            n = 4;
        } else if(r<0.95) {
            n = 5;
        } else {
            n = 6;
        }
        var s: String = "";
        for(i in 0...n) {
            s += createSyllable(rnd);
        }        
        return s;
    }

    public function createWord(rnd: RandomSource): String {
        var lower: String = createRawWord(rnd);
        var first = lower.substr(0,1);
        var remainder = lower.substr(1);
        var name = first.toUpperCase() + remainder;
        return name;
    }

    public function new (worldMetaGen: WorldMetaGen) {
        _worldMetaGen = worldMetaGen;
        WorldMetaGen.cat.catAddGlobalEntity('NameGenerator', this);
    }
}