
package;

import away3d.cameras.*;
import away3d.containers.*;
import away3d.controllers.*;
import away3d.debug.*;
import away3d.entities.Mesh;
import away3d.extrusions.*;
import away3d.lights.*;
import away3d.materials.*;
import away3d.materials.lightpickers.*;
import away3d.materials.methods.*;
import away3d.primitives.*;
import away3d.textures.*;
import away3d.utils.*;

import openfl.display.*;
import openfl.events.*;
import openfl.filters.*;
import openfl.text.*;
import openfl.ui.*;

import openfl.geom.Vector3D;

import streets.StreetPoint;
import streets.Stroke;

/**
 * Experimental class to try out a character.
 * This is a rotating cube character.
 */
class CubeCharacter
     implements ICharacter
{
    private var _rnd: RandomSource;

    private var _mesh:Mesh;
    private var _material:TextureMaterial;

    /*
     * Coordinates are relative to cluster.
     */
    private var _x:Float;
    private var _y:Float;
    private var _z:Float;

    private var _rotationXSpeed:Float;
    private var _rotationYSpeed:Float;
    private var _rotationZSpeed:Float;
    private var _scale:Float;

    /**
     * Meters per second.
     */
    private var _speed:Float;
 
    private var _clusterDesc: ClusterDesc;
    private var _startPoint: StreetPoint;
    private var _prevStart: StreetPoint;
    private var _currentStroke: Stroke;
    private var _targetPoint: StreetPoint;

    /**
     * Contains the last directeion of the character, the length
     * represents the meter per second.
     */
    private var _lastDirection: Vector3D;
    private var _isAB: Bool;

    private var _worldLoader:WorldLoader;

    /**
     * Timestamp of the last behave call.
     */
    private var _lastTime: Float;

    public function characterRemoveMe() : Bool
    {
        return false;
    }


    private function loadNextPoint(previousPoint: StreetPoint)
    {
        /*
         * We select a random stroke and use its destination point.
         */
        var strokes: Array<Stroke> = _startPoint.getAngleArray();

        while(true) {
            var idx = Std.int( _rnd.getFloat() * strokes.length );
            _currentStroke = strokes[idx];
            if( _currentStroke.a == _startPoint ) {
                _isAB = true;
                _targetPoint = _currentStroke.b;
            } else {
                _isAB = false;
                _targetPoint = _currentStroke.a;
            }
            if( strokes.length==1 || _targetPoint!=previousPoint ) {
                break;
            }
        }
    }

    private function loadStartPoint()
    {
        _x = _startPoint.pos.x;
        _y = 0;
        _z = _startPoint.pos.y;
    }


    public function characterBehave(): Void
    {
        var difftime;
        if(false) {
            var now = Sys.time();
            if( _lastTime == 0.0 ) {
                /*
                 * If this is the last call, act if this is 1 standard frame.
                 */
                difftime = 1./50.;
                _lastTime = now;
            } else {
                difftime = now - _lastTime;
                _lastTime = now;
            }
        } else {
            // Currently, we call behave for every logical frame.
            difftime = 1.0/WorldMetaGen.nFrames;
        }

        /*
         * The meters to go.
         */
        var togo: Float = _speed * difftime;

        /*
         * Iterate over movement until we used all the difftime.
         */
        while(togo>0.000001) {

            /*
             * Be sure to have a destination point and compute the vector
             * to it and its length to have the unit vector to it.
             */
            var ux: Float = 0.0; 
            var uy: Float = 0.0 ;
            var dist: Float = 0.0;
            while(true) {
                if( null==_targetPoint ) {
                    loadNextPoint(_prevStart);
                    _prevStart = null;
                }
                var dx = _targetPoint.pos.x - _x;
                var dy = _targetPoint.pos.y - _z;
                dist = Math.sqrt( dx*dx+dy*dy );
                if( dist>0.05 ) {
                    ux = dx/dist;
                    uy = dy/dist;

                    _lastDirection = new Vector3D( ux*_speed, 0., uy*_speed );
                    
                    break;
                }
                _prevStart = _startPoint;
                _startPoint = _targetPoint;
                _targetPoint = null;
                loadStartPoint();
            }

            /*
             * move towards the target with the given speed
             */
            var gonow = togo;

            /*
             * Did we reach the end of the stroke? Then load the next.
             */
            if( gonow > dist ) {
                gonow = dist;
                togo -= gonow;
                _prevStart = _startPoint;
                _startPoint = _targetPoint;
                _targetPoint = null;
                loadStartPoint();
                continue;
            }

            togo -= gonow;
            _x += ux * gonow;
            _z += uy * gonow;
        }

        _y = _worldLoader.worldLoaderGetHeightAt(
            _x + _clusterDesc.x,
            _z + _clusterDesc.z ) + 0.5 + 0.7; // for the street and the aver. radius.


        /*
         * Do not move the mesh yourself, let the character manager do it
         * (which is kind of the world fragment right now)
         * It might change the fragment
         */
        _mesh.rotationX += _rotationXSpeed;
        _mesh.rotationY += _rotationYSpeed;
        _mesh.rotationZ += _rotationZSpeed;
    }


    public function characterGetWorldPos(): Vector3D
    {
        /*
         * We have the coordinates in cluster space. So transform to world
         * space and return.
         */
        return new Vector3D( _x+_clusterDesc.x, _y /* +_clusterDesc.y */, _z+_clusterDesc.z );
    }


    public function characterGetDirection(): flash.geom.Vector3D
    {
        return _lastDirection;
    }


    public function characterGetMesh():Mesh
    {
        return _mesh;
    }


    public function new(
        allEnv: AllEnv,
        clusterDesc0: ClusterDesc,
        startPoint0: StreetPoint )
    {
        _rnd = new RandomSource(clusterDesc0.name);
        _clusterDesc = clusterDesc0;
        _startPoint = startPoint0;
        _targetPoint = null;
        _prevStart = null;
        _lastDirection = new Vector3D(0., 0., 0.);
        loadStartPoint();

        _worldLoader = allEnv.worldLoader;
        var mat = _worldLoader.cubeMaterial;

        /*
         * Set default speed to 10 km/h
         */
        _speed = 2.7 * 5; // 50kmh

        _lastTime = 0.0;

        _rotationXSpeed = Math.random() * 10.00 - 5.00;
        _rotationYSpeed = Math.random() * 10.00 - 5.00;
        _rotationZSpeed = Math.random() * 10.00 - 5.00;
        _scale = 1.; // Math.exp( Math.random()-0.5 );
        _material = mat;
        _mesh = new Mesh(
            new away3d.primitives.CubeGeometry(
                _scale*WorldMetaGen.meter,
                _scale*WorldMetaGen.meter,
                _scale*WorldMetaGen.meter,
                1, 1, 1 ),
            _material );
    }
}

