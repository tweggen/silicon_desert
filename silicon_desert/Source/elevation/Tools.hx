package elevation;

class Tools {

    private static var damping = 0.4;

    /**
     * Compute a reproducable random number for the given 
     * grid position.
     */
    private static function nrand( i: Int, k: Int, x:Int, y:Int ) : Float
    {
        var a:Int, b:Int;
        var n:Int = WorldMetaGen.groundResolution;
        a = i*n;
        b = k*n;
        a += x;
        b += y;
        var ri:Int = (((a*1307+b*11)*(a*3+b*16383)*(a*401+b*19)) & 0xffffff0)>>4;
        var rf:Float = ri / (16.*1024.*1024.);
        return rf;
    }


    public static function refineSkeletonElevation(
        i: Int, k: Int,
        elevationArray:Array<Array<Float>>,
        minElevation:Float, maxElevation:Float,
        x0:Int, y0:Int, x1:Int, y1:Int )
    {
        //trace( "Called minElevation: "+minElevation+", maxElevation: "+maxElevation
        //    +", x0: "+x0+", y0: "+y0+", x1: "+x1+", y1: "+y1 );
        // Return, if there is nothing more to do.

        var xm:Int = Std.int( (x0+x1)/2 );
        var ym:Int = Std.int( (y0+y1)/2 );
        var amplitude = maxElevation-minElevation;
        var bias = -(maxElevation-minElevation)/2.0;


        // Special cases:
        if( (x0+1)>=x1 ) {
            if( (y0+1) >=y1 ) {
                return;
            }
            elevationArray[ym][x0] = 
                (elevationArray[y0][x0]+elevationArray[y1][x0]) / 2.0
                + nrand( i, k,  x0, ym )*amplitude + bias;
            refineSkeletonElevation( i, k, elevationArray,
                minElevation * damping, 
                maxElevation * damping,
                x0, y0, x1, ym );
            refineSkeletonElevation( i, k, elevationArray,
                minElevation * damping, 
                maxElevation * damping,
                x0, ym, x1, y1 );
            return;
        }
        if( (y0+1)>=y1 ) {
            if( (x0+1)>=x1 ) {
                return;
            }
            elevationArray[y0][xm] = 
                (elevationArray[y0][x0]+elevationArray[y0][x1]) / 2.0
                + nrand( i, k,  xm, y0 )*amplitude + bias;
            refineSkeletonElevation( i, k, elevationArray,
                minElevation * damping, 
                maxElevation * damping,
                x0, y0, xm, y0 );
            refineSkeletonElevation( i, k, elevationArray,
                minElevation * damping, 
                maxElevation * damping,
                xm, y0, x1, y0 );
            return;
        }

        elevationArray[y0][xm] = 
            (elevationArray[y0][x0]+elevationArray[y0][x1]) / 2.0
            + nrand( i, k, xm, y0 )*amplitude + bias;
        elevationArray[ym][x0] = 
            (elevationArray[y0][x0]+elevationArray[y1][x0]) / 2.0
            + nrand( i, k,  x0, ym )*amplitude + bias;
        elevationArray[ym][x1] = 
            (elevationArray[y0][x1]+elevationArray[y1][x1]) / 2.0
            + nrand( i, k, x1, ym )*amplitude + bias;
        elevationArray[y1][xm] = 
            (elevationArray[y1][x0]+elevationArray[y1][x1]) / 2.0
            + nrand( i, k, xm, y1 )*amplitude + bias;
        elevationArray[ym][xm] =
            (elevationArray[y0][xm]+elevationArray[y1][xm]
                + elevationArray[ym][x0]+elevationArray[ym][x1]) / 4.0;

        // And generate subdivisions.
        refineSkeletonElevation( i, k, elevationArray,
            minElevation * damping, 
            maxElevation * damping,
            x0, y0, xm, ym );
        refineSkeletonElevation( i, k, elevationArray,
            minElevation * damping, 
            maxElevation * damping,
            xm, y0, x1, ym );
        refineSkeletonElevation( i, k, elevationArray,
            minElevation * damping, 
            maxElevation * damping,
            x0, ym, xm, y1 );
        refineSkeletonElevation( i, k, elevationArray,
            minElevation * damping, 
            maxElevation * damping,
            xm, ym, x1, y1 );

        // That's it.
    }


}