package elevation;

/**
 * Interface to read elevation data.
 *
 * Any elevation operator can read the elevation data "below" it
 * using this interface method.
 */
interface Interface {
    public function getElevationRectBelow( 
        x0: Float, z0: Float, 
        x1: Float, z1: Float 
    ): elevation.Rect;
}
