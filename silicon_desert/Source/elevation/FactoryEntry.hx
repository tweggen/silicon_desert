package elevation;

typedef ElevationEntryFactoryFunction = Dynamic -> elevation.Interface -> Int -> Int -> elevation.CacheEntry;

class FactoryEntry {
    public var context: Dynamic;
    public var layer: String;
    public var factoryFunction: ElevationEntryFactoryFunction;
    public var elevationOperator: elevation.Operator;

    public function new(
        context0: Dynamic,
        layer0: String, 
        factoryFunction0: ElevationEntryFactoryFunction,
        elevationOperator0: elevation.Operator
    ) {
        context = context0;
        layer = layer0;
        elevationOperator = elevationOperator0;
        factoryFunction = factoryFunction0;
    }

}


