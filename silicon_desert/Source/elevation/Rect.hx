package elevation;

/**
 * Carries an rectangular area of elevation data of the world.
 *
 * The resolution
 */
class Rect {
    public var elevations(default, null): Array<Array<Float>>;

    /**
     * The minimal x position of this elevation rectangle.
     * This position is after the previous grid entry, but before or
     * at the first grid entry.
     */
    public var x0: Float;

    /**
     * The minimal z position of this elevation rectangle.
     * This position is after the previous grid entry, but before or
     * at the first grid entry.
     */
    public var z0: Float;

    /**
     * The maximal x position of the elevation rectangle.
     */
    public var x1: Float;

    /**
     * the maximal x position of this elevation rectangle.
     */
    public var z1: Float;

    /**
     * An arbitrary origin to define the raster resolution.
     * The raster size is defined by this point as raster point #0
     * and the raster point #10000.
     */
    public var xGrid0(default,null): Float;

    /**
     * An arbitrary origin to define the raster resolution.
     * The raster size is defined by this point as raster point #0
     * and the raster point #10000.
     */
    public var zGrid0(default,null): Float;

    /**
     * An arbitrary origin to define the raster resolution.
     * The raster size is defined by raster point #0
     * and this raster point as #10000.
     */
    public var xGrid10k(default,null): Float;

    /**
     * An arbitrary origin to define the raster resolution.
     * The raster size is defined by raster point #0
     * and this raster point as #10000.
     */
    public var zGrid10k(default,null): Float;

    /**
     * The number of horizontal raster points publically available in this grid.
     */
    public var nHoriz(default,null): Int;

    /**
     * The number of vertical raster points publically available in this grid.
     */
    public var nVert(default,null): Int;

    public function new (
        nHoriz0: Int, nVert0: Int
    ) {
        x0 = 0.0;
        z0 = 0.0;
        x1 = 0.0;
        z1 = 0.0;
        xGrid0 = 0.0;
        zGrid0 = 0.0;
        xGrid10k = 10000.;
        zGrid10k = 10000.;
        nHoriz = nHoriz0;
        nVert = nVert0;
        elevations = [for (y in 0...nVert) [for (x in 0...nHoriz) 0]];
    }
}