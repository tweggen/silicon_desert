package elevation;

/**
 * Hold one fragment size of the terrain. This is used internally be the
 * terrain pipeline to cache the intermediates.
 */
class CacheEntry {    
    private var _worldMetaGen: WorldMetaGen;
    public var elevations: Array<Array<Float>> = null;


    /**
     * Return the interpolated height at the given relative position.
     */
    public function getHeightAt( x0:Float, z0:Float ):Float
    {
        if (null == elevations) {
            throw 'elevation.CacheEntry.getHeightAt(): elevations still is null.';
        }

        var groundResolution = WorldMetaGen.groundResolution;
        var elevationStepSize:Float =
            WorldMetaGen.fragmentSize / WorldMetaGen.groundResolution;

        var ex = Std.int( (x0+WorldMetaGen.fragmentSize/2.0) / elevationStepSize );
        var ey = Std.int( (z0+WorldMetaGen.fragmentSize/2.0) / elevationStepSize );

        if( ex<0 || ex>groundResolution ) {
            throw( 'Invalid ex: $ex > groundResolution: $groundResolution, x0: $x0' );
        }
        if( ey<0 || ey>groundResolution ) {
            throw( 'Invalid ex: $ey > groundResolution: $groundResolution, z0: $z0' );
        }

        /*
         * Now compute, where exactly within the triangles we are.
         */
        var y00:Float = elevations[ey][ex];
        var y01:Float = elevations[ey][ex+1];
        var y10:Float = elevations[ey+1][ex];
        var y11:Float = elevations[ey+1][ex+1];

        var tileX:Float = (x0+WorldMetaGen.fragmentSize/2.0)-(elevationStepSize*ex);
        var tileY:Float = (z0+WorldMetaGen.fragmentSize/2.0)-(elevationStepSize*ey);

        // Which of the triangles is it?
        var yResult:Float;
        if( (tileX+tileY) <= elevationStepSize ) {
            // Upper half.
            yResult = y00;
            yResult += (y01-y00)*tileX/elevationStepSize;
            yResult += (y10-y00)*tileY/elevationStepSize;
        } else {
            // Lower half, inversed calculation.
            yResult = y11;
            yResult += (y10-y11)*(elevationStepSize-tileX)/elevationStepSize;
            yResult += (y01-y11)*(elevationStepSize-tileY)/elevationStepSize;
        }

        return yResult;
    }


    public function new ( worldMetaGen: WorldMetaGen ) 
    {
        _worldMetaGen = worldMetaGen;
    }
}
