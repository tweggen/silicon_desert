package elevation;

class LayerAdapter
    implements elevation.Interface
{
    public var layer(default, null): String;
    private var _elevationCache: elevation.Cache;


    public function getElevationRectBelow( 
        x0: Float, z0: Float, 
        x1: Float, z1: Float 
    ): elevation.Rect {
        return _elevationCache.elevationCacheGetRectBelow( x0, z0, x1, z1, layer );
    }


    public function new (
        elevationCache: elevation.Cache,
        layer0: String
    ) {
        _elevationCache = elevationCache;
        layer = layer0;
    }
}
