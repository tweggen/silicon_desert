
package;

import streets.Generator;
import streets.Stroke;
import streets.StreetPoint;


/**
 * Information about one given cluster. This one creates and gathers more
 * information about a specific cluster.
 */
class ClusterDesc 
    extends Entity
{
    public var merged:Bool;
    public var x:Float = 0.;
    public var y:Float = 0.;
    public var z:Float = 0.;
    public var size:Float = 100.;
    public var name:String = "Unnamed";

    public var averageHeight = 0.0;

    private var _arrCloseCities:Array<ClusterDesc>;
    private var _nClosest:Int;
    private var _maxClosest:Int = 5;
    private var _strKey: String;

    // TXWTODO: Move these associatioins to the entity catalogue

    /** 
     * Each cluster has a stroke store associated that descirbes the 
     * street graph.
     */
    private var _strokeStore: streets.StrokeStore;

    /**
     * Street generator will be initialized on demand.
     */
    private var _streetGenerator: streets.Generator;

    /**
     * In addition, each cluster has a lot generator associated.
     */
    private var _quarterGenerator: streets.QuarterGenerator;

    /**
     * This is the store for all quarters we generated.
     */
    private var _quarterStore: streets.QuarterStore;

    public function getKey() : String{
        return _strKey;
    }


    public function getClosest() : Array<ClusterDesc>
    {
        return _arrCloseCities;
    }


    public function addClosest( other:ClusterDesc )
    {
        if( other==this ) return;

        var distance:Float = Math.sqrt(
            (other.x-this.x)*(other.x-this.x) 
            +(other.z-this.z)*(other.z-this.z) );

        // Special case first.
        if( 0==_nClosest ) {
            _arrCloseCities[0] = other;
            _nClosest = 1;
            return;
        }

        // Now insert, whereever required
        var idx:Int=0;
        while( idx<_nClosest ) {
            var cl:ClusterDesc = _arrCloseCities[idx];
            // Also ignore this if already known.
            if( cl==other ) {
                idx++;
                return;
            }

            var clDist:Float = Math.sqrt(
                (cl.x-this.x)*(cl.x-this.x) 
                +(cl.z-this.z)*(cl.z-this.z) );

            if( distance<clDist ) {
                // Smaller distance? Then insert myself here.
                var idx2:Int = idx+1;
                var max:Int = _nClosest+1;
                if( max>_maxClosest ) max = _maxClosest;
                while( idx2<max ) {
                    _arrCloseCities[idx2] = _arrCloseCities[idx2-1];
                    ++idx2;
                }
                _arrCloseCities[idx] = other;
                _nClosest = max;
                // Inserted.
                return;
            }
            idx++;
        }
    }


    /**
     * Using the information about the next cities, create seed points for 
     * the map based on the interconnecting stations.
     */
    public function _addHighwayTriggers() {
        // TXWTODO: Do it acually
        {
            var newA = new StreetPoint();
            newA.setPos( -size/2., -size/2. );
            var newB = new StreetPoint();
            var stroke = Stroke.createByAngleFrom( newA, newB, Math.PI*0.25, 20., true, 1.5 );
            _streetGenerator.addStartingStroke(stroke);
        }
        {
            var newA = new StreetPoint();
            newA.setPos( size/2., -size/2. );
            var newB = new StreetPoint();
            var stroke = Stroke.createByAngleFrom( newA, newB, 3.*Math.PI*0.25, 20., true, 1.5 );
            _streetGenerator.addStartingStroke(stroke);
        }
        {
            var newA = new StreetPoint();
            newA.setPos( -size/2., size/2. );
            var newB = new StreetPoint();
            var stroke = Stroke.createByAngleFrom( newA, newB, -Math.PI*0.25, 20., true, 1.5 );
            _streetGenerator.addStartingStroke(stroke);
        }
        {
            var newA = new StreetPoint();
            newA.setPos( size/2., size/2. );
            var newB = new StreetPoint();
            var stroke = Stroke.createByAngleFrom( newA, newB, -3.0*Math.PI*0.25, 20., true, 1.5 );
            _streetGenerator.addStartingStroke(stroke);
        }
    }


    private function triggerStreets(): Void {
        if( null==_streetGenerator ) {
            _strokeStore = new streets.StrokeStore();
            _streetGenerator = new streets.Generator();
            _quarterStore = new streets.QuarterStore();
            _quarterGenerator = new streets.QuarterGenerator();

            _streetGenerator.reset( "streets-"+_strKey, _strokeStore );
            _streetGenerator.setBounds( -size/2., -size/2., size/2., size/2. );
            _addHighwayTriggers();
            _streetGenerator.generate();

            _quarterGenerator.reset( "quarters-"+_strKey, _quarterStore, _strokeStore );
            _quarterGenerator.generate();
        }
    }


    public function streetGenerator(): streets.Generator {
        triggerStreets();
        return _streetGenerator;
    }


    public function quarterGenerator(): streets.QuarterGenerator {
        triggerStreets();
        return _quarterGenerator;
    }


    public function strokeStore(): streets.StrokeStore {
        triggerStreets();
        return _strokeStore;
    }


    public function quarterStore(): streets.QuarterStore {
        triggerStreets();
        return _quarterStore;
    }


    public function new (strKey: String) {
        _strKey = strKey;
        _arrCloseCities = new Array<ClusterDesc>();
        _nClosest = 0;
        merged = false;
        // Street generator will be initialized on demand.
        _streetGenerator = null;
    }
}

