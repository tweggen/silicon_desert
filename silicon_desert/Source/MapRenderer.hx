
package;

import openfl.display.Sprite;
import openfl.geom.Rectangle;
import openfl.geom.Matrix;

class MapRenderer {

    // var _colMapFill = 0x4444cc;
    var _colMapFill = 0x224433;
    var _colMapBorder = 0x668877;
    var _colCity = 0x88aa99;
    var _colConnection = 0xaaccbb;
    var _colHairCross = 0x080833;

    var _allEnv:AllEnv;
    var _sprCities: Sprite;
    var _haveCities: Bool;
    var _isFullscreen: Bool;

    var _sprMap: Sprite;

    var _virtWidth:Float = 1000.0;
    var _mapScale:Float;

    public var mapWidth: Int = 200;
    var _fWidth:Float = 200.0;
    var _iWidth:Int = 200;

    public function mapRendererUpdateCities()
    {
        if(_haveCities) {
            trace("Don't have cities.");
            return;
        }
        var clusterList = _allEnv.worldMetaGen.metaGenGetClusterDescIn(
            -_allEnv.worldMetaGen.maxWidth/2.0,
            _allEnv.worldMetaGen.maxWidth/2.0,
            -_allEnv.worldMetaGen.maxHeight/2.0,
            _allEnv.worldMetaGen.maxHeight/2.0
        );

        var h = _iWidth/2;
        _sprMap.graphics.beginFill(_colMapFill,0.3);
        _sprMap.graphics.lineStyle(4.0, _colMapBorder);
        _sprMap.graphics.drawRect(2-h,2-h,_iWidth-4,_iWidth-4);
        _sprMap.graphics.endFill();
        _sprMap.graphics.lineStyle(1.0);
        // _sprCities.graphics.lineStyle(8.0, 0x222277);
        // _sprCities.graphics.drawRect(4,4,996,996);
        // _sprCities.graphics.lineStyle(1.0);

        for(cluster in clusterList) {
            var cx:Float = _allEnv.worldMetaGen.maxWidth/2.0 + cluster.x;
            var cy:Float = _allEnv.worldMetaGen.maxHeight/2.0 + cluster.z;
            var csh:Float = cluster.size/2.0;
            // trace('_mapScale = $_mapScale');
            cx *= _mapScale;
            cy *= _mapScale;
            csh *= _mapScale;
            cy = _virtWidth-cy;
            var closest = cluster.getClosest();
            var max = 4;
            for(next in closest) {
                --max;

                var bx:Float = _allEnv.worldMetaGen.maxWidth/2.0 + next.x;
                var by:Float = _allEnv.worldMetaGen.maxHeight/2.0 + next.z;
                bx *= _mapScale;
                by *= _mapScale;
                by = _virtWidth-by;

                _sprCities.graphics.lineStyle(3.0, 0xaaccbb, 0.5);
                _sprCities.graphics.moveTo(cx,cy);
                _sprCities.graphics.lineTo(bx,by);

                if(max<0) break;
            }
        }

        for(cluster in clusterList) {
            var cx:Float = _allEnv.worldMetaGen.maxWidth/2.0 + cluster.x;
            var cy:Float = _allEnv.worldMetaGen.maxHeight/2.0 + cluster.z;
            var csh:Float = cluster.size/2.0;
            // trace('_mapScale = $_mapScale');
            cx *= _mapScale;
            cy *= _mapScale;
            csh *= _mapScale;
            cy = _virtWidth-cy;
            // cx = _virtWidth-cx;
            _sprCities.graphics.beginFill(0x88aa99, 0.5);
            _sprCities.graphics.lineStyle(2.0, 0x88aa99, 0.5);
            _sprCities.graphics.drawRect(
                cx-csh, cy-csh,
                csh*2, csh*2);
            _sprCities.graphics.endFill();
            // trace('Marking city: (${cx-csh}, ${cy-csh}) - (${cx+csh}, ${cy+csh})');

        }

        _sprMap.graphics.lineStyle(1.0, _colHairCross);
        _sprMap.graphics.moveTo(4-h, 0);
        _sprMap.graphics.lineTo(h-4,0);
        _sprMap.graphics.moveTo(0, 4-h);
        _sprMap.graphics.lineTo(0,h-4);
        
        _haveCities = true;
    }

    public function mapRendererMovePlayer(x: Float, y: Float, ori: Float)
    {
        x += _allEnv.worldMetaGen.maxWidth/2.0; // now ranges from 0..50000
        y += _allEnv.worldMetaGen.maxWidth/2.0;
        x *= _mapScale; // 0..virtWith (1000)
        y *= _mapScale;
        var h = _fWidth/2;
        y = _virtWidth-y;
        if(x<h) x = h;
        if(y<h) y = h;
        if(x>(_virtWidth-h)) x = _virtWidth - h;
        if(y>(_virtWidth-h)) y = _virtWidth - h;

        var m:Matrix = new Matrix();
        m.translate(-x,-y);
        m.rotate(Math.PI-ori*Math.PI/180.);
        _sprCities.transform.matrix = m;
    }

    public function mapRendererGetCities(): Sprite
    {
        return _sprMap;
    }

    public function new (allEnv:AllEnv) 
    {
        _allEnv = allEnv;
        _sprCities = new Sprite();
        _sprMap = new Sprite();
        _sprMap.addChild(_sprCities);
        _sprCities.x = 0;
        _sprCities.y = 0;
        _sprMap.scrollRect = new Rectangle(
            -_iWidth/2, -_iWidth/2, _iWidth, _iWidth);

        _haveCities = false;
        _mapScale = _virtWidth / _allEnv.worldMetaGen.maxWidth;
    }
}
