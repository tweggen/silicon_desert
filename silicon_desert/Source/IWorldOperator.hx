package;

interface IWorldOperator {
    public function worldOperatorApply(
        worldMetaGen: WorldMetaGen): Void;

    // public function worldOperatorGetElevation( 
    //    ): Array<Array<Float>>; 
}