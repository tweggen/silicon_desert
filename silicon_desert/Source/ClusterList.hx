package;

class ClusterList extends Entity
{
    private var _worldMetaGen: WorldMetaGen;
    private var _listClusters: List<ClusterDesc>;

    /**
     * Return a list of clusters.
     * TODO: Make a nonmodifiable list.
     */
    public function getClusterList(): List<ClusterDesc> {
        return _listClusters;
    }

    /**
     * Add a new cluster to the list of cluster. 
     * To keep everything consistent, this method is responsible for adding
     * the cluster to the catalogue.
     */
    public function addCluster(clusterDesc: ClusterDesc) {
        _listClusters.add(clusterDesc);
        WorldMetaGen.cat.catAddEntity("ClusterDesc", clusterDesc);
    }

    public function new(worldMetaGen: WorldMetaGen) {
        _worldMetaGen = worldMetaGen;
        WorldMetaGen.cat.catAddGlobalEntity('ClusterList', this);
        _listClusters = new List<ClusterDesc>();
    }
}