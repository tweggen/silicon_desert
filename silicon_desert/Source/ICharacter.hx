import away3d.entities.Mesh;

import openfl.geom.Vector3D;

interface ICharacter
{
    /**
     * Decide, wether this character shall be removed after the current 
     * behave iteration. If this returns true, the character will be removed
     * from the scene and from the list of characters.
     */
    public function characterRemoveMe():Bool;

    /**
     * Called every frame to control behavior.
     */
    public function characterBehave():Void;

    public function characterGetMesh():Mesh;

    /**
     * Return the absolute position of this character. Depending on its position,
     * it will be added to the proper fragment or be discarded.
     */
    public function characterGetWorldPos(): Vector3D;

    /**
     * Return the current direction of the character.
     */
    public function characterGetDirection(): Vector3D;

}

