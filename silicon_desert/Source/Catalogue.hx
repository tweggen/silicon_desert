package;

#if neko
import neko.vm.Mutex;
#else
#if cpp
import cpp.vm.Mutex;
#else
import sys.thread.Mutex;
#end
#end

typedef SingletonFactory = Void->Dynamic;

/**
 * A catalogue keeps all entities for a given scope.
 *
 * Usual scopes are:
 * - world (maintained by metagen)
 * - fragment (maintained by fragment)
 * - player (maintained by player actor, i.e. main)
 *
 * A catalogue keeps lists all entities owned by the the respective scope.
 * Entities should be referenced by id only outside of the catalogue.
 *
 * MetaGen maintains the currently valid catalogues and provides
 * lookup functions for everyone.
 *
 * It is recommended to prefix the entities with the actual entity class.
 */
class Catalogue {

    static var _nextEntityIndex: haxe.Int64;

    static var _mutexMap: Mutex;

    static var MAGIC_CREATING = "CREATING";

    private var _mapEntities: Map<String, Dynamic>;
    private var _mapSingletons: Map<String, SingletonFactory>;

    /**
     * Create a singleton object of the given type. If the singleton type was
     * not known before, the object is created and stored. Otherwise
     * the previously created instance directly is returned.
     */
    public function catGetSingleton(name: String, factory: SingletonFactory): Dynamic {
        try {
            _mutexMap.acquire();

            var doCreate = false;
            if(_mapSingletons.exists(name)) {
            } else {
                _mapSingletons[name] = factory;
                doCreate = true;
            }
            _mutexMap.release();


            // Immediately register it.
            var obj: Dynamic;
            if( doCreate ) {
                obj = factory();
            } else {
                obj = null;
            }

            // TXWTODO: This is a huge race condition!!!!
            // Maybe we create a singleton creation queue?

            _mutexMap.acquire();
            var id = "singleton-" + name;
            if(doCreate) {
                _mapEntities[id] = obj;
             } else {
                obj = _mapEntities[id];
             }
            _mutexMap.release();

            return obj;
        } catch( unknown: Dynamic ) {
            trace( "Unknown exception: "+Std.string( unknown )+ "\n"
                + haxe.CallStack.toString( haxe.CallStack.callStack() )
                + haxe.CallStack.toString( haxe.CallStack.exceptionStack() ) );
        }
        return null;

    }

    /**
     * Add a new item to the item of objects.
     */
    public function catAddEntity(type: String, obj: Dynamic): String {
        try {
            _mutexMap.acquire();
            var id: String;
            do {
                id = type + "-" + haxe.Int64.toStr(_nextEntityIndex);
                ++_nextEntityIndex;
                haxe.Int64.add(_nextEntityIndex, haxe.Int64.ofInt(1));
            } while(_mapEntities.exists(id));
            _mapEntities[id] = obj;
            obj.id = id;
            _mutexMap.release();
            return id;
        } catch( unknown: Dynamic ) {
            trace( "Unknown exception: "+Std.string( unknown )+ "\n"
                + haxe.CallStack.toString( haxe.CallStack.callStack() )
                + haxe.CallStack.toString( haxe.CallStack.exceptionStack() ) );
        }
        return "";
    }

    /**
     * Add an entity to the system with a globally known name.
     */
    public function catAddGlobalEntity(name: String, obj: Dynamic): String {
        try {
            _mutexMap.acquire();
            if(_mapEntities.exists(name)) {
                _mutexMap.release();
                throw 'Catalogue: Global entity "$name" already exists.';
            }
            _mapEntities[name] = obj;
            // obj.id = name;
            _mutexMap.release();
            return name;
        } catch( unknown: Dynamic ) {
            trace( "Unknown exception: "+Std.string( unknown )+ "\n"
                + haxe.CallStack.toString( haxe.CallStack.callStack() )
                + haxe.CallStack.toString( haxe.CallStack.exceptionStack() ) );
        }
        return "";
    }

    public function catRemoveEntity(id: String): Void {
        var obj: Dynamic;
        _mutexMap.acquire();
        if(_mapEntities.exists(id)) {
            _mapEntities.remove(id);
            _mutexMap.release();
        } else {
            _mutexMap.release();
            throw 'Catalogue: Unknown entity "$id".';
        }
    }

    public function catGetEntity(id:String): Dynamic {
        try {
            var obj: Dynamic;
            _mutexMap.acquire();
            if(_mapEntities.exists(id)) {
                obj = _mapEntities[id];
            } else {
                obj = null;
            }
            _mutexMap.release();
            return obj;
        } catch( unknown: Dynamic ) {
            trace( "Unknown exception: "+Std.string( unknown )+ "\n"
                + haxe.CallStack.toString( haxe.CallStack.callStack() )
                + haxe.CallStack.toString( haxe.CallStack.exceptionStack() ) );
        }
        return null;
    };

    public function new() {
        _nextEntityIndex = haxe.Int64.make(0xCAFEBABE, 0x00000000);
        _mapEntities = new Map<String, Dynamic>();
        _mapSingletons = new Map<String, SingletonFactory>();
        _mutexMap = new Mutex();
    }
}
