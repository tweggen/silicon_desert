
package materials;

import away3d.materials.*;

class StreetMaterial
    extends Entity

{
    private var _allEnv: AllEnv;
    private var _matStreet: TextureMaterial;

    public function texGet(): TextureMaterial {
        return _matStreet;
    }

    public function new(terrainEnv: AllEnv)
    {
        _allEnv = terrainEnv;
        //_matStreet = new TextureMaterial(away3d.utils.Cast.bitmapTexture("building/stealtiles1.jpg"));
        _matStreet = new TextureMaterial(
            away3d.utils.Cast.bitmapTexture("street/streets1to4.png")
            // away3d.utils.Cast.bitmapTexture("building/yellowwindows.png")
        );
        _matStreet.lightPicker = _allEnv.allEnvironmentGetLightPicker();
        _matStreet.ambientColor = 0xffffff;
        _matStreet.ambient = 0.5;
        _matStreet.specular = 0;
        _matStreet.repeat = true;
        _matStreet.smooth = false;
        _matStreet.addMethod( _allEnv.allEnvironmentGetFogMethod() );
    }
}