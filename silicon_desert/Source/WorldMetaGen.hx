

package;


import openfl.Assets;
import openfl.events.Event;
import openfl.events.ProgressEvent;
import openfl.net.URLLoader;
import openfl.net.URLLoaderDataFormat;
import openfl.display.Loader;
import openfl.display.LoaderInfo;

/**
 * Generates world properties as a source for the fragment generators.
 *
 * Main procedural properties also are generated in this class, such as 
 * the main clusters.
 */
class WorldMetaGen 
{
    public static var nFrames: Int = 50;

    /** 
     * What is the unit of a single meter.
     */
    public static var meter:Float = 1.;

    /**
     * What is the size of a single computed fragment.
     */
    public static var fragmentSize:Float = meter*400;

    /**
     * This is our seed. It also will be used for other sub-parts of
     * the world-
     */
    private var _myKey:String;

    /**
     * The maximal geometrix extend (will be centered).
     *
     * Let's take 50 kilometers.
     */
    public var maxWidth(default,null):Float;
    public var maxHeight(default,null):Float;


    public var minElevation:Float = 100.0;
    public var maxElevation:Float = -10.0;
    public static var groundResolution:Int = 0;
    public var globalLiquidHeight:Float = -1000000;

    private var _fragmentOperatorTree: FragmentOperatorTree;

    private var _worldOperators:List<IWorldOperator>;


    private var _rnd:RandomSource;

    private var _traceLoadAsset = false;

    public static var cat(default,null): Catalogue;

    /** 
     * Return all clusters found in the specific 2d extent.
     */
    public function metaGenGetClusterDescIn( xmin:Float, xmax:Float, zmin:Float, zmax:Float ) : List<ClusterDesc>
    {
        var listMatchingClusters = new List<ClusterDesc>();
        var clusterList: ClusterList = cast(cat.catGetEntity("ClusterList"), ClusterList);
        var nMatches:Int = 0;
        for(cl in clusterList.getClusterList()) {
            if( cl.x>=xmin && cl.x<xmax && cl.z>=zmin && cl.z<zmax ) {
                listMatchingClusters.add( cl );
                ++nMatches;
            }
        }
        // trace('WorldMetaGen:metaGenGetClusterDescIn(): Returning $nMatches matches');
        return listMatchingClusters;
    }


    /**
     * This is the seed of this world.
     */
    public function metaGenSeed():String
    {
        return _myKey;
    }


    public function metaGenAddFragmentOperator(op:IFragmentOperator):Void
    {
        _fragmentOperatorTree.addFragmentOperator(op);
    }


    /**
     * Return our singleton ship object.
     */
    public function metaGenGetShip() : away3d.containers.ObjectContainer3D
    {
        var o3dShip = cast( cat.catGetEntity( "3d:ship" ), away3d.containers.ObjectContainer3D );
        o3dShip.scaleX = 0.17;
        o3dShip.scaleY = 0.7;
        o3dShip.scaleZ = 0.5;
        return o3dShip;
    }


    /**
     * Execute all world operators for this metagen.
     * This can be terrain generatation, cluster generation etc. .
     */
    private function applyWorldOperators():Void
    {
        trace("WorldMetaGen: Calling world operators...");
        for(o in _worldOperators) {
            o.worldOperatorApply(this);
        }
        trace("WorldMetaGen: Done calling world operators.");
    }


    public function applyFragmentOperators(
        allEnv: AllEnv,
        fragment: WorldFragment):Void
    {
        trace('WorldMetaGen: Calling fragment operators for ${fragment.getId()}...');
        _fragmentOperatorTree.apply( function (fo) { fo.fragmentOperatorApply(allEnv, fragment); });
        trace('WorldMetaGen: Done calling fragment operators for ${fragment.getId()}...');
    }
    

    public function new ( strKey:String, onComplete:Void->Void )
    {
        maxHeight = 30000.;
        maxWidth = 30000.;

        groundResolution = Std.int( fragmentSize / 20.0 );

        cat = new Catalogue();

        // TXWTODO: Where to generate a number of globals?
        {
            var nameGenerator = new NameGenerator(this);
        }
        {
            var groundOperator = new GroundOperator(this, strKey);
        }
        {
            var elevationCache = new elevation.Cache(this);
            var elevationBaseFactory = new ops.elevation.ElevationBaseFactory(this);
            elevationCache.elevationCacheRegisterElevationOperator(
                elevation.Cache.LAYER_BASE+"/000002/fillGrid", 
                elevationBaseFactory);
        }

        _myKey = strKey;
        _rnd = new RandomSource( _myKey );

        _fragmentOperatorTree = new FragmentOperatorTree(this);
        _worldOperators = new List<IWorldOperator>();

        /*
         * Create a world operator that finds and creates clusters 
         * for the cities.
         */
        _worldOperators.add(new ops.world.GenerateClustersOperator(_myKey));

        /*
         * Create a fragment operator that reads the elevations after 
         * the elevation pipeline and creates a ground mesh.
         */
        metaGenAddFragmentOperator(new ops.fragment.CreateTerrainMeshOperator(_myKey));

        /*
         * One time operations: Apply all world operators.
         */
        applyWorldOperators();

        away3d.loaders.parsers.Parsers.enableAllBundled();

        // TXWTODO: I feel all 3d loading doesn't belong here.
        loaders.Load3ds.load( "ship.3ds", "3d:ship", onComplete );
    }
}