package;

import away3d.cameras.*;
import away3d.containers.*;
import away3d.controllers.*;
import away3d.core.base.*;
import away3d.debug.*;
import away3d.entities.Mesh;
import away3d.extrusions.*;
import away3d.filters.BloomFilter3D;
import away3d.filters.DepthOfFieldFilter3D;
import away3d.lights.*;
import away3d.materials.*;
import away3d.materials.lightpickers.*;
import away3d.materials.methods.*;
import away3d.primitives.*;
import away3d.textures.*;
import away3d.utils.*;

import openfl.display.*;
import openfl.events.*;
import openfl.filters.*;
import openfl.text.*;
import openfl.ui.*;
import openfl.Assets;


import openfl.utils.Timer;

class Main extends Sprite
{
    private var _isPaused = true;

    private var _colHudText: Int = 0xbbddcc;

    /**
     * This is the actual repreesentation of the world that we genereate.
     */ 
    private var _worldMetaGen:WorldMetaGen;

    /**
     * The logical world state.
     * All operators operate on this world state.
     */
    private var _worldLoader:WorldLoader;

    //engine variables
    private var _scene:Scene3D;
    

    //scene objects
    private var _text:TextField;
    
    //rotation variables
    private var _move:Bool = false;
    private var _lastShipPanAngle:Float;
    private var _lastTiltAngle:Float;
    private var _lastMouseX:Float;
    private var _lastMouseY:Float;
    
    //_movement variables
    private var _drag:Float = 0.5;
    // Let's specify this is km/h.
    private var _walkIncrement:Float = (60/3.6) * WorldMetaGen.meter / WorldMetaGen.nFrames;
    private var _strafeIncrement:Float = (20/3.6) * WorldMetaGen.meter / WorldMetaGen.nFrames;
    private var _turnIncrement: Float = 2;
    private var _walkSpeed:Float = 0;
    private var _strafeSpeed:Float = 0;
    private var _turnSpeed:Float = 0;
    private var _walkAcceleration:Float = 0;
    private var _strafeAcceleration:Float = 0;
    private var _turnAcceleration:Float = 0;
    private var _walkFast:Bool = false;
    private var _doShoot:Bool = false;
    private var _doNextCity:Bool = false;
    private var _doPrevCity:Bool = false;
    
    private var camera:Camera3D;
    private var view:View3D;
    //private var cameraController:FirstPersonController;
    private var _hoverCameraController:HoverController;
    private var _awayStats:AwayStats;
    private var _doShowAwayStats: Bool = false;
    private var _objShip:ObjectContainer3D;

    private var _textDebug:TextField;

    private var _terrainEnv:AllEnv;

    private var _shipPanAngle:Float;
    private var _camPanAngle:Float;
    private var _camTiltAngle:Float;

    private var _bmComp:Bitmap;
    private var _sprComp:Sprite;
    private var _sprMap:Sprite;

    /**
     * Absolute time of last frame in milliseconds.
     */
    private var _timeLastFrame: Int;

    /**
     * How large should OSD elements be scaled?
     * For smaller mobile displays, we assume the number to be 2.4,
     * for large high-res desktop screens, we think of rather 0.8.
     * Although this is gonna change.
     */
    private var _osdScale: Float;


    private var _lastClosestCluster: ClusterDesc;

    private var _joinCharacter = false;

    /**
     * Create an instructions overlay.
     * Currently used for debugging only.
     */
    private function init_text():Void
    {
        var fontName: String = Assets.getFont("Prototype.ttf").fontName;
        // var fontName: String = "_sans";
        _textDebug = new TextField();
        _textDebug.defaultTextFormat = new TextFormat(fontName, Std.int(15. *_osdScale), 0xbbddcc);
        _textDebug.width = 300. * _osdScale;
        _textDebug.height = 200. * _osdScale;
        _textDebug.selectable = false;
        _textDebug.mouseEnabled = false;
        _textDebug.text = "systems activated.";
        
        addChild(_textDebug);
    }
    

    /**
     * Initialise the listeners
     */
    private function initListeners():Void
    {
        addEventListener(Event.ENTER_FRAME, onEnterFrame);
        stage.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
        stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
        stage.addEventListener(Event.RESIZE, onResize);
        stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
        stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
        onResize();
    }
    

    private function onNewClosestCluster(
        closestCluster: ClusterDesc,
        secondClosestCluster: ClusterDesc
    ): Void {
        var closestDist: Float;
        {
            var cluster = closestCluster;
            var dx = cluster.x-_objShip.x;
            var dz = cluster.z-_objShip.z;
            closestDist = Math.sqrt(dx*dx+dz*dz);
        }
        var secondClosestDist;
        {
            var cluster = secondClosestCluster;
            var dx = cluster.x-_objShip.x;
            var dz = cluster.z-_objShip.z;
            secondClosestDist = Math.sqrt(dx*dx+dz*dz);
        }

    }


    private var firstTime = 20;
    /**
     * Navigation and render loop
     */

    /*
     * Count the actual elapsed time to know who much and when to 
     * behave.
     */
    private var _dtCount: Float = 0.0;
    private var _nBehaveFrames: Int = 1;
    private function onEnterFrame( event:Event ) : Void
    {
        if(firstTime>0) {
            --firstTime;
            onResize();
        }
        /*
         * How much time did elapse since the last actuation?
         */
        var timeCurrentFrame = openfl.Lib.getTimer();
        var deltaTime;
        if(_timeLastFrame != 0) {
            deltaTime = timeCurrentFrame - _timeLastFrame;
        } else {
            deltaTime = 1;
        }
        _timeLastFrame = timeCurrentFrame;
        _dtCount += deltaTime/1000.0;
        /* 
         * We keep a virtual number of frames a 1/25s to express
         * the duration of time since the last frame. This should
         * keep animations at stable speed even with lags.
         */
        var nBehaveFrames: Int = Std.int( _dtCount / (1.0/WorldMetaGen.nFrames) );
        // trace( '_dtCount = $_dtCount' );
        // trace( 'nBehaveFrames = $nBehaveFrames' );
        _dtCount -= (1.0/WorldMetaGen.nFrames) * nBehaveFrames;
        // Let behavior run for each logical frame.
        for( behaveIdx in 0...nBehaveFrames ) {

            /*
             * Compute target values from mouse.
             *
             * TXWTODO: Keep them separately from keyboard.
             */
            if( _move ) {
                if( _isPressLeft ) {
                    var diffY = stage.mouseY - _lastMouseY;
                    if( diffY > 0.0 ) {
                        if( diffY > 100.0 ) {
                            diffY = 100.0;
                        }
                        _walkAcceleration = (diffY * _walkIncrement) / -100.;
                    } else if( diffY < 0.0 ) {
                        if( diffY < -100.0 ) {
                            diffY = -100.0;
                        }
                        _walkAcceleration = (diffY * _walkIncrement) / -100.;
                    }
                    _shipPanAngle = 0.3*(stage.mouseX - _lastMouseX) + _lastShipPanAngle;
                } else {
                    //_camPanAngle = 0.3*(stage.mouseX - _lastMouseX) + lastPanAngle;
                    _camTiltAngle = 0.3*(stage.mouseY - _lastMouseY) + _lastTiltAngle;
                }
            }

            /*
             * Apply turn acceleration and 
             */
            if( _turnSpeed != 0 || _turnAcceleration != 0 ) {
                _turnSpeed = (_turnSpeed + _turnAcceleration )*_drag;
                if( Math.abs( _turnSpeed ) < 0.01 ) {
                    _turnSpeed = 0;
                }
                _shipPanAngle += _turnSpeed;
                // cameraController.panAngle += _turnSpeed;
            }

            while( _shipPanAngle > 3600.0 ) {
                _shipPanAngle -= 3600.0;
            }
            while( _shipPanAngle < -3600.0 ) {
                _shipPanAngle += 3600.0;
            }

            var dirX:Float = -Math.sin( _shipPanAngle*Math.PI/180.0 );
            var dirZ:Float = -Math.cos( _shipPanAngle*Math.PI/180.0 );

            if (_walkSpeed != 0 || _walkAcceleration != 0) {
                _walkSpeed = (_walkSpeed + _walkAcceleration*(_walkFast?2:1))*_drag;
                if (Math.abs(_walkSpeed) < 0.01)
                    _walkSpeed = 0;
            }
            
            if (_strafeSpeed != 0 || _strafeAcceleration != 0) {
                _strafeSpeed = (_strafeSpeed + _strafeAcceleration*(_walkFast?2:1))*_drag;
                if (Math.abs(_strafeSpeed) < 0.01)
                    _strafeSpeed = 0;
            }

            _objShip.x += dirX * _walkSpeed;
            _objShip.z += dirZ * _walkSpeed;
            // trace( 'walkSpeed = $_walkSpeed' );

            var heightAtShip = _worldLoader.worldLoaderGetHeightAt( _objShip.x, _objShip.z );

            var heightShipTarget = heightAtShip + 2.0;
            var heightShipMinimum = heightAtShip + 1.0;
            _objShip.y += 0.5 * ( heightShipTarget - _objShip.y );
            if(_objShip.y < heightShipMinimum ) {
                _objShip.y = heightShipMinimum;
            }
            // We need that later to move the cam
            var effectiveShipHeight = _objShip.y;

            _objShip.rotationY = _shipPanAngle /* -90.0 */;
            _objShip.rotationZ = _objShip.rotationZ*0.9 + 0.1*_turnAcceleration;

            /*
             * The camera must not be lower than the slope between ship and camera.
             * We should consider some height offsets, though.
             */
            var heightAtCamera = _worldLoader.worldLoaderGetHeightAt( camera.x, camera.z );
            var effectiveCameraHeight = heightAtCamera + 3.0;
            {
                var dy = effectiveShipHeight - effectiveCameraHeight;
                var theta = Math.atan(dy/_hoverCameraController.distance) * 180. /Math.PI;
                // trace('Main: theta is $theta.'); 
                _hoverCameraController.tiltAngle = -theta;
            }
            _hoverCameraController.panAngle = _shipPanAngle;
            _hoverCameraController.lookAtPosition.x = _objShip.x;
            // Look 2 meters above the ship.
            _hoverCameraController.lookAtPosition.y = _objShip.y + 1.0;
            _hoverCameraController.lookAtPosition.z = _objShip.z;

            _sprComp.rotation = -_shipPanAngle;

            // _objShip.x = camera.x;
            // _objShip.y = camera.y;
            // _objShip.z = camera.z;
            // _objShip.rotationX = camera.rotationX;
            // _objShip.rotationY = camera.rotationY;
            // _objShip.rotationZ = camera.rotationZ;

            _worldLoader.worldLoaderBehave();
            _hoverCameraController.update();
        }

        // And update everything else at most once a frame.
        if( nBehaveFrames > 0 ) {
            // We won't warp that fast that we need to provide in each and every frame.
            _worldLoader.worldLoaderProvideFragments( _objShip.x, _objShip.y, _objShip.z );
            _terrainEnv.mapRenderer.mapRendererMovePlayer(_objShip.x, _objShip.z, _shipPanAngle);

            {
                /*
                 * Compute distance to closest clusters.
                 */
                var clusterList: ClusterList = 
                    cast(
                        WorldMetaGen.cat.catGetEntity("ClusterList"),
                        ClusterList);
                var closestCluster: ClusterDesc = null;
                var secondClosestCluster: ClusterDesc = null;
                var closestDist = 10000000000.0;
                var secondClosestDist = 10000000000.0;
                for( cluster in clusterList.getClusterList() ) {
                    var dx = cluster.x-_objShip.x;
                    var dz = cluster.z-_objShip.z;
                    var dist = Math.sqrt(dx*dx+dz*dz);
                    if (dist < closestDist) {
                        // trace(dist);
                        secondClosestCluster = closestCluster;
                        secondClosestDist = closestDist;
                        closestDist = dist;
                        closestCluster = cluster;
                    } else if(dist < secondClosestDist) {
                        secondClosestCluster = cluster;
                        secondClosestDist = dist;
                    }
                }
                closestDist = Math.floor(closestDist/10.)/100.;
                secondClosestDist = Math.floor(secondClosestDist/10.)/100.;

                if( closestCluster != null ) {
                    if( _lastClosestCluster != closestCluster ) {
                        _lastClosestCluster = closestCluster;
                        onNewClosestCluster(closestCluster, secondClosestCluster);
                    }
                }

                /*
                 * Update info test.
                 */
                var strNewInfo:String = 
                    'navigation activated\n'
                    // +'x: ${_objShip.x}, z: ${_objShip.z}\n'
                    +'${closestCluster.name} (${closestDist}km)';
                if (secondClosestCluster != null) {
                    strNewInfo += '\n${secondClosestCluster.name} (${secondClosestDist}km)';
                }
                if( _textDebug.text != strNewInfo ) {
                    _textDebug.text = strNewInfo;
                }
            }
        }

        view.render();
    }
    
    /**
     * Key down listener for camera control
     */
    private function onKeyDown( event:KeyboardEvent ):Void
    {
        switch (event.keyCode) {
        case Keyboard.SHIFT:
            _walkFast = true;
        case Keyboard.UP, Keyboard.W:
            _walkAcceleration = _walkIncrement;
        case Keyboard.DOWN, Keyboard.S:
            _walkAcceleration = -_walkIncrement;
        case Keyboard.A:
            _strafeAcceleration = -_strafeIncrement;
        case Keyboard.D:
            _strafeAcceleration = _strafeIncrement;
        case Keyboard.J:
            _doPrevCity = true;
        case Keyboard.K:
            _doNextCity = true;
        case Keyboard.LEFT:
            _turnAcceleration = -_turnIncrement;
        case Keyboard.RIGHT:
            _turnAcceleration = _turnIncrement;
        case Keyboard.CONTROL:
            _doShoot = true;
        case Keyboard.P: // Print
            _doShowAwayStats = !_doShowAwayStats;
            _awayStats.visible = _doShowAwayStats;
            // Sys.println(Profiler.getCallStackResultsAsText());
        case Keyboard.O: // Onboard
            _joinCharacter = !_joinCharacter;
        }

        /*
         * Evaluate actions triggered by key press.
         */
        if( _doShoot ) {
            _doShoot = false;
            /* var character = new ProjecRayCharacter(
                _worldLoader,
                _objShip.x, _objShip.y, _objShip.z,
                _objShip.rotationX, _objShip.rotationY, _objShip.rotationZ );
            _worldLoader.addCharacter( character ); */
        }

        var nextCity: ClusterDesc = null;

        if( _doNextCity ) {
            _doNextCity = false;
            if( _lastClosestCluster != null ) {
                var clusterList: ClusterList = cast(
                    WorldMetaGen.cat.catGetEntity("ClusterList"), ClusterList );
                // Set to our cluster if we found it.
                var myCluster: ClusterDesc = null;
                for( cluster in clusterList.getClusterList() ) {
                    if( null != myCluster ) {
                        trace('Found myCluster');
                        nextCity = cluster;
                        break;
                    }
                    if( cluster == _lastClosestCluster ) {
                        trace('Found lastClosestCluster');
                        myCluster = cluster;
                    }
                }
                if(nextCity==null) {
                    nextCity = clusterList.getClusterList().first();
                }
            }
        }
        if( _doPrevCity ) {
            _doPrevCity = false;
        }

        if( nextCity != null ) {
            _objShip.x = nextCity.x;
            _objShip.z = nextCity.z;
        }
    }
    
    /**
     * Key up listener for camera control
     */
    private function onKeyUp( event:KeyboardEvent ):Void
    {
        switch (event.keyCode) {
        case Keyboard.SHIFT:
            _walkFast = false;
        case Keyboard.UP, Keyboard.W, Keyboard.DOWN, Keyboard.S:
            _walkAcceleration = 0;
        case Keyboard.A, Keyboard.D:
            _strafeAcceleration = 0;
        case Keyboard.LEFT, Keyboard.RIGHT:
            _turnAcceleration = 0;
        }
    }
    
    /*
     * TXWTODO: _move this to a reusable controller.
     * We use the left half of the screen as controller 
     * The right half changes view angle.
     */

    private var _isPressLeft:Bool;     

    /**
     * Mouse down listener for navigation
     */
    private function onMouseDown( event:MouseEvent ):Void
    {
        stage.addEventListener( Event.MOUSE_LEAVE, onStageMouseLeave );

        _lastMouseX = stage.mouseX;
        _lastMouseY = stage.mouseY;

        _move = true;
        _lastShipPanAngle = _shipPanAngle;
        _lastTiltAngle = _camTiltAngle;
        if( stage.mouseX < (stage.stageWidth/2) ) {
            _isPressLeft = true;
        } else {
            _isPressLeft = false;
        }

    }
    
    /**
     * Mouse up listener for navigation
     */
    private function onMouseUp( event:MouseEvent ):Void
    {
        _move = false;
        if( _isPressLeft ) {
            _walkAcceleration = 0;
        }
        stage.removeEventListener( Event.MOUSE_LEAVE, onStageMouseLeave );
    }
    

    /**
     * Mouse stage leave listener for navigation
     */
    private function onStageMouseLeave( event:Event ):Void
    {
        _move = false;
        if( _isPressLeft ) {
            _walkAcceleration = 0;
        }
        stage.removeEventListener(Event.MOUSE_LEAVE, onStageMouseLeave);
    }
    
    /**
     * stage listener for resize events
     */
    private function onResize( event:Event = null ):Void
    {
        view.width = stage.stageWidth;
        view.height = stage.stageHeight;
        var d = stage.stageWidth/20;
        _awayStats.x = stage.stageWidth - _awayStats.width - d;
        _awayStats.y = d;
        _sprComp.width = stage.stageHeight/4.0;
        _sprComp.height = stage.stageHeight/4.0;
        _sprComp.x = (stage.stageWidth)/2.0;
        _sprComp.y = (stage.stageHeight)/2.0;

        {
            var h = stage.stageHeight/5.0;
            var mw = _terrainEnv.mapRenderer.mapWidth;
            _sprMap.x = d;
            _sprMap.y = d; //stage.stageHeight-x-mw;
            // _sprMap.width = h;
            // _sprMap.height = h;
            _textDebug.x = d;
            _textDebug.y = 300;
        }
    }

    /**
     * Initialise the 3d engine.
     * Initializes
     * - openfl stage
     * - away3d view
     * - camera
     * - head up display
     */
    private function initEngine():Void
    {
        /*
         * openfl stage
         */
        stage.scaleMode = StageScaleMode.NO_SCALE;
        stage.align = StageAlign.TOP_LEFT;

        /*
         * away3d view
         */
        view = new View3D();
        _scene = view.scene;
        camera = view.camera;
        
        camera.lens.far = 1000.0;
        camera.lens.near = 1;
        camera.y = 2*WorldMetaGen.meter;
        camera.x = 0;
        camera.z = 0;
        var perspectiveLens = cast( camera.lens, away3d.cameras.lenses.PerspectiveLens);
        perspectiveLens.fieldOfView = 60;


        addChild(view);

        // var dof:DepthOfFieldFilter3D = new DepthOfFieldFilter3D( 32, 32 );
        // dof.range = 20*WorldMetaGen.meter;

        view.filters3d = [ new BloomFilter3D( 200, 200, .85, 2, 2)];

        _awayStats = new AwayStats(view);
        _awayStats.scaleX = _osdScale;
        _awayStats.scaleY = _osdScale;
        _awayStats.visible = _doShowAwayStats;
        addChild(_awayStats);

        var bitmapData = Assets.getBitmapData("comp.png");
        _bmComp = new Bitmap(bitmapData);
        _bmComp.alpha = 0.6;
        _bmComp.x = -255.5;
        _bmComp.y = -255.5;
        _sprComp = new Sprite();
        // _sprComp.addChild( _bmComp );
        addChild( _sprComp );
    }


#if 0
    private function drawTestMap() {
        var spr = this; //new Sprite();
        // spr.width = 400.;
        // spr.height = 400.;
        // spr.x = 5008.;
        // spr.y = 0.;
        // addChild(spr);
        var colors = [ 0x0000ff, 0x00ff00, 0x00ffff, 0xff0000, 0xff00ff, 0xffff00, 0xffffff];
        var streetGenerator = cast( 
            WorldMetaGen.cat.catGetEntity('streets.Generator'), 
            streets.Generator );
        var strokes = streetGenerator.getStrokes();
        trace('Main: Drawing ${strokes.length} streets.');
        var i = 0;
        for( stroke in strokes ) {
            var ax = stroke.a.pos.x;
            var ay = stroke.a.pos.y;
            var bx = stroke.b.pos.x; 
            var by = stroke.b.pos.y; 
                
            // trace('Stroke: ($ax, $ay) - ($bx, $by).');
            spr.graphics.lineStyle(stroke.weight*4.0, colors[i%7]);
            spr.graphics.moveTo( ax + 400, 1000-ay );
            spr.graphics.lineTo( bx + 400, 1000-by );
            ++i;
        } 
    }
#end

    /**
     * (internal) called as oon the world meta structure has been 
     * initialized. Initializes all the practical issues.
     */
    private function onMetaGenComplete() : Void
    {
        initEngine();

        init_text();

        _worldLoader = new WorldLoader( 
            _worldMetaGen, _scene );
        _terrainEnv = _worldLoader.terrainEnv();

        {
            /*
             * To create the map, we need the terrainEnv, as it owns the maprenderer.
             * However, the sprite probably is not available yet.
             */
            _terrainEnv.mapRenderer.mapRendererUpdateCities();
            _sprMap = _terrainEnv.mapRenderer.mapRendererGetCities();
            addChild( _sprMap);
        }

        /*
         * Initialize the various event listeners.
         */
        initListeners();

        /*
         * Load the area suitable for the camera position.
         * This might _move both the world and the camera.
         * This is an initial load of the data required.
         */
        _worldLoader.worldLoaderProvideFragments( camera.x, camera.y, camera.z );

        /*
         * Add the preliminary player.
         */
        var objShip = _worldMetaGen.metaGenGetShip();
        var mesh1:Mesh;
        var nChildren:Int;
        nChildren = objShip.numChildren;
        //trace( "model loaded nChildren: "+objShip.numChildren );
        for( i in 0...nChildren ) {
            mesh1 = cast( objShip.getChildAt(i), Mesh );
            if( null != mesh1 ) {
                mesh1.material.lightPicker =
                    _worldLoader.terrainEnv().allEnvironmentGetLightPicker();
            }
            // mesh1.material = new ColidorMaterial(0xFF0000);
        }
        _objShip = objShip;
        _scene.addChild( objShip );
        _objShip.x = 0.0;
        _objShip.y = 100.0;
        _objShip.z = 0.0;

        //setup controller to be used on the camera
        _hoverCameraController = new HoverController( camera, null, 180.0, 0.0, 8 ); 
        // cameraController = new FirstPersonController( camera, 180, 0, -80, 80 );
        _hoverCameraController.steps = 25;
        _hoverCameraController.tiltAngle = 0;
        //_hoverCameraController.minTiltAngle = 0;

        _camPanAngle = 0.0;
        _camTiltAngle = 0.0;
        _shipPanAngle = 0.0;

        _terrainEnv.mapRenderer.mapRendererUpdateCities();

        // drawTestMap();
    }


    /**
     * Global initialise function
     */
    private function init():Void
    {
        _worldMetaGen = new WorldMetaGen( "mydear", onMetaGenComplete );
    }


    //public function onLibLoaded( lib )
   // {
   //     haxe.Timer.delay( init, 1000 );
   // }

    
    /**
     * Constructor
     */
    public function new()
    {
        super();
        // lime.utils.Log.level = lime.utils.Log.LogLevel.VERBOSE;

#if desktop
        _osdScale = 1.0;
#else
        _osdScale = 2.5;
#end
        _bmComp = null;
        _timeLastFrame = 0;
        _lastClosestCluster = null;
        //Assets.loadLibrary( "default", onLibLoaded );
        //haxe.Timer.delay( init, 1000 );
        init();
    }
    

}
